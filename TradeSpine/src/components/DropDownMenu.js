/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {
  TouchableOpacity,
  View,
  Image,
  TextInput,
  StyleSheet,
} from 'react-native';
import BottomSheetOptions from '../components/BottomSheetOptions';

const DropDownMenu = props => {
  const bottomSheetRef = React.useRef(null);
  const [text, setText] = React.useState('');

  const toggleSheet = type => {
    bottomSheetRef.current?.open();
  };

  const onSelectType = item => {
    setText(item.name);
    props.onSelectItem(item);
    bottomSheetRef.current.close();
  };

  return (
    <View style={{flex: 1, marginHorizontal: 14, ...props.parentStyle}}>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          borderWidth: 1,
          marginTop: 25,
          borderColor: '#F5818D',
          borderRadius: 5,
          fontSize: 16,
          width: '100%',
          height: 46,
          flex: 1,
          ...props.inputStyle,
        }}
        onPress={() => toggleSheet()}>
        <View style={{width: '80%'}}>
          <TextInput
            style={{
              fontSize: 14,
              paddingStart: 12,
              color: 'black',
            }}
            onChangeText={setText}
            value={text}
            editable={false}
            placeholder={props.placeholderText}
          />
        </View>
        <View style={{width: '10%', marginStart: 10, ...props.style}}>
          <Image
            source={
              props.icon ? props.icon : require('../assets/images/dropdown.png')
            }
            style={[styles.ImageStyle]}
          />
        </View>
      </TouchableOpacity>
      <BottomSheetOptions
        ref={bottomSheetRef}
        onSelectOption={type => onSelectType(type)}
        options={props.options}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 14,
  },
  ImageStyle: {
    height: 20,
    width: 20,
    margin: 12,
    tintColor: '#C0C0C0',
  },
});

export default DropDownMenu;
