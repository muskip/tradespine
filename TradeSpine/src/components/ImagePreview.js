/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
  Dimensions,
} from 'react-native';
import {CommonConstants} from '../constants/index';

export const ImagePreview = ({
  style,
  item,
  imageKey,
  onPress,
  index,
  active,
  local,
  containerWidth,
}) => {
  const width = Dimensions.get('window').width;

  return (
    <TouchableOpacity style={[styles.videoContainer, {width: containerWidth}]}>
      <View style={[styles.imageContainer, styles.shadow, {flex: 1}]}>
        <Image
          resizeMode={'contain'}
          style={[
            styles.videoPreview,
            active
              ? {
                  width: containerWidth - 10,
                  height: width / 2 - 20,
                  resizeMode: 'contain',
                }
              : {
                  width: containerWidth - 10,
                  height: width / 2 - 20,
                  resizeMode: 'contain',
                },
          ]}
          source={{
            uri: `${CommonConstants.IMAGE_URL}/ads_banner/${
              item.ads_image_name ? item.ads_image_name : item
            }`,
          }}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  videoContainer: {
    paddingVertical: 14,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 20,
  },
  videoPreview: {
    height: 160,
    borderRadius: 8,
    resizeMode: 'cover',
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
});
