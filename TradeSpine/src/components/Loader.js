/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {ActivityIndicator, View} from 'react-native';

const Loader = props => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
      }}>
      {props.isLoading && <ActivityIndicator size="large" color="#0000ff" />}
    </View>
  );
};

export default Loader;
