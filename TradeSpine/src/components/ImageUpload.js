import * as React from 'react';
import {TouchableOpacity, View, Image, StyleSheet, Text} from 'react-native';
import FastImage from 'react-native-fast-image';

function ImageUpload({photoLabel, onPress, selectedImage, style}) {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={[localStyles.image_container, {...style}]}>
      <View style={localStyles.image_hint_container}>
        <Image
          style={localStyles.add_icon}
          source={require('../assets/images/add_image.png')}
        />
      </View>
      <FastImage
        source={{uri: selectedImage && selectedImage.uri}}
        style={[localStyles.selected_image]}
        resizeMethod={FastImage.resizeMode.contain}
      />
    </TouchableOpacity>
  );
}

const localStyles = StyleSheet.create({
  image_container: {
    width: '31%',
    height: undefined,
    aspectRatio: 1 / 1,
    borderColor: '#DEDFE4',
    borderWidth: 1,
    borderRadius: 5,
    alignContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: '#F2F3F5',
    overflow: 'hidden',
  },
  add_icon: {height: 30, width: 30},
  image_hint_container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selected_image: {width: '100%', height: '100%'},
});

export default ImageUpload;
