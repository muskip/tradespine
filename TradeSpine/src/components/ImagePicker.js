/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import RBSheet from 'react-native-raw-bottom-sheet';
import {View, TouchableOpacity, Image, Text} from 'react-native';

const ImagePicker = React.forwardRef((props, ref) => {
  return (
    <RBSheet ref={ref} height={162}>
      <View style={{paddingHorizontal: 22, paddingVertical: 12}}>
        <Text>{props.label ? props.label : 'Select an option'}</Text>
        <View style={{justifyContent: 'space-around', flexDirection: 'row'}}>
          <View style={{width: '50%', padding: 30, paddingTop: 35}}>
            <TouchableOpacity
              onPress={() => {
                ref.current.close();
                setTimeout(() => {
                  props.onCameraSelect();
                }, 500);
              }}
              style={{
                width: '100%',
                alignItems: 'center',
              }}>
              <Image
                style={{
                  width: 34,
                  height: 34,
                  marginStart: 3,
                  marginBottom: 10,
                }}
                source={require('../assets/images/use-camera.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  textTransform: 'uppercase',
                  color: '#96999E',
                }}>
                {'Use Camera'}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{width: '50%', padding: 30, paddingTop: 40}}>
            <TouchableOpacity
              onPress={() => {
                ref.current.close();
                setTimeout(() => {
                  props.onLibrarySelect();
                }, 500);
              }}
              style={{
                width: '100%',
                alignItems: 'center',
              }}>
              <Image
                style={{
                  width: 34,
                  height: 29,
                  marginStart: 3,
                  marginBottom: 10,
                }}
                source={require('../assets/images/gallery.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  textTransform: 'uppercase',
                }}>
                {'Gallery'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </RBSheet>
  );
});

export default ImagePicker;
