import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
} from 'react-native';
import {CommonConstants} from '../constants/index';

export default function AdsImage({item}) {
  // console.log('item',item);
  return (
    <View style={[styles.imageContainer, styles.shadow]}>
      <Image
        style={[styles.videoPreview, {height: 120}]}
        source={{
          uri: `${CommonConstants.IMAGE_URL}/ads_banner/${item.ads_image_name}`,
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  videoPreview: {
    width: '95%',
    height: 155,
    resizeMode: 'cover',
  },
  desc: {
    fontSize: 14,
    letterSpacing: 0,
    lineHeight: 24,
    marginTop: 18,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 10,
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
});
