/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {TouchableOpacity, View, Text, ScrollView} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';

const BottomSheetOptions = React.forwardRef((props, ref) => {
  return (
    <RBSheet ref={ref} closeOnPressBack>
      <ScrollView>
        <View
          style={{
            padding: 25,
            alignItems: 'center',
          }}>
          {props.options.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  props.onSelectOption(item);
                }}
                style={{
                  flexDirection: 'row',
                  marginTop: index === 0 ? 0 : 15,
                }}>
                <Text style={{fontSize: 18}}>{item.name}</Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
    </RBSheet>
  );
});

export default BottomSheetOptions;
