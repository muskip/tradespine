export const CommonConstants = {
  IMAGE_URL: 'https://www.tradespine.co.in/assets',
  USER_TOKEN: 'user_token',
  USER_DATA: 'user_data',
  USER_ID: 'user_id',
};
