import {applyMiddleware, combineReducers, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import * as reducers from '../reducer';

const middleware = [thunk, __DEV__ && createLogger({collapsed: true})].filter(
  Boolean,
);
const appReducer = combineReducers({...reducers});
export const store = createStore(appReducer, applyMiddleware(...middleware));
