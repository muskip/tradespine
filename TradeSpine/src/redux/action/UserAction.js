import {USER_ACTION_TYPES} from '../types';

export const signIn = (token, user, userId) => {
  return {
    type: USER_ACTION_TYPES.SIGNIN,
    payload: {
      token,
      user,
      userId,
    },
  };
};

export const restoreUser = (token, user, userId) => {
  return {
    type: USER_ACTION_TYPES.RESTORE_USER,
    payload: {
      token,
      user,
      userId,
    },
  };
};

export const signOut = () => {
  return {
    type: USER_ACTION_TYPES.SIGNOUT,
    payload: {},
  };
};
