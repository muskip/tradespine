import {USER_ACTION_TYPES} from '../types';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonConstants} from '../../constants';

const initialState = {
  isLoggedIn: false,
  userToken: null,
  user: null,
  userId: null,
};

class AuthReducer {
  static reduce(state = initialState, action) {
    if (AuthReducer[action.type]) {
      return AuthReducer[action.type](state, action);
    } else {
      return state;
    }
  }

  static [USER_ACTION_TYPES.RESTORE_USER](state, action) {
    return {
      ...state,
      isLoggedIn: false,
      userToken: action.payload.token,
      user: action.payload.user,
      userId: action.payload.userId,
    };
  }

  static [USER_ACTION_TYPES.SIGNIN](state, action) {
    AsyncStorage.setItem(
      CommonConstants.USER_TOKEN,
      JSON.stringify(action.payload.token),
    );
    AsyncStorage.setItem(
      CommonConstants.USER_DATA,
      JSON.stringify(action.payload.user),
    );
    AsyncStorage.setItem(
      CommonConstants.USER_ID,
      JSON.stringify(action.payload.userId),
    );
    return {
      ...state,
      isLoggedIn: true,
      userToken: action.payload.token,
      user: action.payload.user,
      userId: action.payload.userId,
    };
  }

  static [USER_ACTION_TYPES.SIGNOUT](state, action) {
    AsyncStorage.removeItem(CommonConstants.USER_TOKEN);
    AsyncStorage.removeItem(CommonConstants.USER_DATA);
    AsyncStorage.removeItem(CommonConstants.USER_ID);
    return {
      ...state,
      isLoggedIn: false,
      userToken: null,
      user: null,
      userId: null,
    };
  }
}

export default AuthReducer.reduce;
