import {store} from '../redux/store';

export const getUserId = () => {
  const userId = store.getState() && store.getState().auth.userId;
  return userId;
};

const getHeader = () => {
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
};

const getHeaderFormData = () => {
  return {
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data',
  };
};

export default class APIRequest {
  static get = (url, callback, showError = true) => {
    fetch(url, {
      method: 'GET',
      headers: getHeader(),
    })
      .then(response => {
        console.log("res", response);
        if (response.status == 401) {
          // throw error if server does not return response for 401
          // throw new Error('Unauthorized Access');
        }
        return response.json();
      })
      .then(responseJson => {
        return callback(responseJson, undefined);
      })
      .catch(error => {
        console.log('get: error ', error);
        if (showError) {
          // showErrorToast(Strings.default_rest_error);
        }
        return callback(undefined, error.message);
      });
  };

  static post = (url, body, callback, showError = true) => {
    fetch(url, {
      method: 'POST',
      headers: getHeader(),
      body: JSON.stringify(body),
    })
      .then(response => response.json())
      .then(responseJson => {
        return callback(responseJson, undefined);
      })
      .catch(error => {
        console.log('post: error ', error);
        if (showError) {
          // showErrorToast(Strings.default_rest_error);
        }
        return callback(undefined, error.message);
      });
  };

  static put = (url, body, callback, showError = true) => {
    fetch(url, {
      method: 'PUT',
      headers: getHeader(),
      body: JSON.stringify(body),
    })
      .then(response => response.json())
      .then(responseJson => {
        return callback(responseJson, undefined);
      })
      .catch(error => {
        console.log('put: error ', error);
        if (showError) {
          // showErrorToast(Strings.default_rest_error);
        }
        return callback(undefined, error.message);
      });
  };

  static delete = (url, callback, showError = true) => {
    fetch(url, {
      method: 'DELETE',
      headers: getHeader(),
    })
      .then(response => response.json())
      .then(responseJson => {
        return callback(responseJson, undefined);
      })
      .catch(error => {
        console.log('delete: error ', error);
        if (showError) {
          // showErrorToast(Strings.default_rest_error);
        }
        return callback(undefined, error.message);
      });
  };

  static postFormData = (url, body, callback, showError = true) => {
    fetch(url, {
      method: 'POST',
      headers: getHeaderFormData(),
      body: body,
    })
      .then(response => response.json())
      .then(responseJson => {
        return callback(responseJson, undefined);
      })
      .catch(error => {
        console.log('postFormData: error ', error);
        if (showError) {
          // showErrorToast(Strings.default_rest_error);
        }
        return callback(undefined, error.message);
      });
  };
}
