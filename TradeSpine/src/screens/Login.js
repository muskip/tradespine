/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import APIRequest from '../network/APIRequest';
import {isTextEmpty} from '../utils/index';
import CheckBox from '@react-native-community/checkbox';
import {useDispatch} from 'react-redux';
import {signIn} from '../redux/action/UserAction';
import Loader from '../components/Loader';

function Login(props) {
  const dispatch = useDispatch();
  const [userName, setUserName] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [isAcceptConditions, setIsAcceptConditions] = React.useState(false);
  const [isLoading, setLoading] = React.useState(false);
  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginHorizontal: 16,
          marginTop: 30,
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{padding: 5}}
          onPress={() => props.navigation.goBack()}>
          <Image
            source={require('../assets/images/leftArrow.png')}
            style={{height: 24, width: 24}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{padding: 5}}
          onPress={() => props.navigation.navigate('Signup')}>
          <Text style={{fontSize: 18, color: '#F68592'}}>Register</Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          margin: 20,
        }}>
        <Text style={{fontSize: 20, fontWeight: 'bold'}}>
          Login to your account
        </Text>
        <View style={[styles.input, {marginTop: 40}]}>
          <Image
            source={require('../assets/images/user.png')}
            style={styles.ImageStyle}
          />
          <TextInput
            style={{fontSize: 18, paddingStart: 5, top: 3}}
            onChangeText={setUserName}
            value={userName}
            placeholder={'Username'}
          />
        </View>
        <View style={styles.input}>
          <Image
            source={require('../assets/images/lock.png')}
            style={styles.ImageStyle}
          />
          <TextInput
            style={{fontSize: 18, paddingStart: 5, top: 3}}
            onChangeText={setPassword}
            value={password}
            placeholder={'Password'}
          />
        </View>
        <View style={{width: '100%'}}>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 14, color: '#C1C0BA', textAlign: 'right'}}>
              Forgot password?
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 20,
            }}>
            <CheckBox
              disabled={false}
              value={isAcceptConditions}
              onValueChange={newValue => setIsAcceptConditions(newValue)}
            />
            <Text style={{fontSize: 14, color: '#393939', marginTop: 5}}>
              I accept terms and conditions.
            </Text>
          </View>
        </View>
        <TouchableOpacity style={styles.button} onPress={onLogin}>
          <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
            Login
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );

  function onLogin() {
    if (isTextEmpty(userName)) {
      Alert.alert('', 'Please enter username');
      return;
    }
    if (isTextEmpty(password)) {
      Alert.alert('', 'Please enter password');
      return;
    }
    if (!isAcceptConditions) {
      Alert.alert('', 'Please accept terms and conditions');
      return;
    }
    setLoading(true);
    var formdata = new FormData();
    formdata.append('username', userName);
    formdata.append('password', password);
    console.log('formdata', formdata);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/login',
      formdata,
      response => {
        if (response) {
          setLoading(false);
          console.log('response', response);
          if (response.tokenid) {
            const user = response.result;
            const token = response.tokenid;
            const userId = response.result.userid;
            dispatch(signIn(token, user, userId));
          }
        }
      },
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    width: '100%',
    borderColor: '#CCC9C3',
    borderRadius: 8,
    flexDirection: 'row',
    height: 46,
  },
  button: {
    alignItems: 'center',
    padding: 10,
    marginTop: 10,
    borderRadius: 8,
    width: '100%',
    height: 44,
    backgroundColor: '#BA2525',
  },
  ImageStyle: {
    height: 25,
    width: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 12,
    marginStart: 10,
    tintColor: '#AFAFB0',
  },
  checkbox: {
    alignSelf: 'center',
  },
});

export default Login;
