import React from 'react';
import {WebView} from 'react-native-webview';
import {CommonActions} from '@react-navigation/native';

export default function Webview(props) {
  const orderId = props.route.params.orderId;
  const userId = props.route.params.userId;

  return (
    <WebView
      source={{
        uri: `https://www.tradespine.co.in/welcome/confirm_payment?userid=${userId}&order_id=${orderId}`,
      }}
      onLoadProgress={e => {
        const event = e.nativeEvent;
        if (
          event.url.includes(
            'https://www.tradespine.co.in/welcome/thankyou_order',
          )
        ) {
          setTimeout(() => {
            props.navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [{name: 'Home'}],
              }),
            );
          }, 5000);
        }
      }}
    />
  );
}
