/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  FlatList,
  ScrollView,
  Dimensions,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import APIRequest from '../network/APIRequest';
import {CommonConstants} from '../constants/index';
import Loader from '../components/Loader';
import {FlatListSlider} from 'react-native-flatlist-slider';
import {ImagePreview} from '../components/ImagePreview';
import AdsImage from '../components/AdsImage';
import {getAdsIndex} from '../utils/index';

function Timeline(props) {
  const userId = useSelector(state => state.auth.userId);
  const [timeline, setTimeline] = React.useState([]);
  const [topHeaderItemsList, setTopHeaderItemsList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(false);
  const [timelineAdList, setTimelineAdsList] = React.useState([]);
  const width = Dimensions.get('window').width;
  const [adsCurrentIndex, setAdsCurrentIndex] = React.useState(null);
  let tempList = [];
  let sum = 0;
  const isShowMoreAds = adsCurrentIndex
    ? timelineAdList.length > adsCurrentIndex + 1
    : timelineAdList.length
    ? timelineAdList.length > adsCurrentIndex + 1
    : false;
  console.log('isShowMoreAds', isShowMoreAds, adsCurrentIndex);
  // console.log('timelineAdList', timelineAdList);

  function getTimelineAds() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/timeline_ads',
      formdata,
      response => {
        if (response) {
          // console.log(' timeline adsss--->>>>>', response);
          setLoading(false);
          setTimelineAdsList(response?.result);
        }
      },
    );
  }

  useEffect(() => {
    getTopHeaderData();
    getTimeline();
    getTimelineAds();
  }, []);

  function getTopHeaderData() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/top_ads_timeline',
      formdata,
      response => {
        if (response) {
          // console.log('response top_ads_wholeseller', response);
          setLoading(false);
          setTopHeaderItemsList(response?.result);
        }
      },
    );
  }

  function getTimeline() {
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/timeline',
      formdata,
      response => {
        if (response) {
          // console.log('response timeline', response);
          setTimeline(response?.result);
        }
      },
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <ScrollView>
        {topHeaderItemsList && topHeaderItemsList.length > 0 && (
          <View style={{flex: 1}}>
            <View>
              <FlatListSlider
                data={topHeaderItemsList}
                timer={5000}
                component={<ImagePreview containerWidth={width - 10} />}
                indicator={false}
                contentContainerStyle={{paddingHorizontal: 5}}
              />
            </View>
          </View>
        )}
        {timeline && timeline.length > 0 && (
          <View style={{flex: 1}}>
            <FlatList
              data={timeline}
              contentContainerStyle={{
                marginBottom: 20,
                backgroundColor: '#FDE3E6',
                marginHorizontal: 10,
                borderRadius: 10,
                padding: 10,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item, index}) => {
                let adsIndex = null;
                const i = index > 0 ? 3 : 2;
                sum = sum + i;
                tempList.push(sum);
                const isAdsIndex = tempList.includes(index);
                if (isAdsIndex) {
                  adsIndex = getAdsIndex(index);
                  if (timelineAdList[adsIndex]) {
                    setAdsCurrentIndex(adsIndex);
                  }
                }
                return isAdsIndex && timelineAdList[adsIndex] ? (
                  <AdsImage item={timelineAdList[adsIndex]} />
                ) : (
                  <View
                    style={[
                      styles.shadow,
                      {
                        flex: 1,
                        flexDirection: 'column',
                        margin: 1,
                        backgroundColor: '#FFFF',
                        borderRadius: 10,
                        marginHorizontal: 5,
                        marginTop: 10,
                        padding: 10,
                      },
                    ]}>
                    <Image
                      resizeMode={'contain'}
                      style={[
                        styles.imageThumbnail,
                        {
                          width: '100%',
                          height: 150,
                          backgroundColor: '#f6f2ef',
                        },
                      ]}
                      source={{
                        uri: item.timeline_image
                          ? `${CommonConstants.IMAGE_URL}/images/product_image/${item.timeline_image}`
                          : `${CommonConstants.IMAGE_URL}/images/sellers.jpg`,
                      }}
                    />
                    <View style={{marginHorizontal: 10, marginTop: 20}}>
                      <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                        {`${item?.timeline_title?.toUpperCase() ?? ''}`}
                      </Text>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Image
                          source={require('../assets/images/location.png')}
                          style={{
                            width: 16,
                            height: 16,
                            alignSelf: 'center',
                            tintColor: '#a9a499',
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 14,
                            marginStart: 2,
                            color: '#b8b1a6',
                          }}>
                          {`${item?.address_line2 ?? ''} ${
                            item?.address_line1 ?? ''
                          }`}
                        </Text>
                      </View>
                      <Text
                        style={{
                          fontSize: 18,
                          marginTop: 10,
                        }}>
                        {'Member since 11 days'}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginTop: 10,
                          color: 'black',
                        }}>
                        {'Description'}
                      </Text>
                      <Text
                        style={{
                          fontSize: 14,
                          marginVertical: 10,
                          color: '#b8b1a6',
                        }}>
                        {`${item?.timeline_description ?? ''}`}
                      </Text>
                    </View>
                  </View>
                );
              }}
            />
          </View>
        )}
        <View
          style={{
            backgroundColor: '#FDE3E6',
            marginBottom: 20,
            paddingHorizontal: 16,
            paddingBottom: 10,
            marginHorizontal: 12,
          }}>
          {isShowMoreAds &&
            timelineAdList &&
            timelineAdList.length &&
            timelineAdList.map((item, index) => {
              // console.log('item', item);
              // console.log('adsCurrentIndex', adsCurrentIndex);
              if (adsCurrentIndex) {
                if (index > adsCurrentIndex) {
                  return <AdsImage item={timelineAdList[index]} />;
                }
              } else {
                return <AdsImage item={timelineAdList[index]} />;
              }
            })}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 80,
    color: 'black',
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Timeline;
