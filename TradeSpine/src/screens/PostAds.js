/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput,
  Alert,
} from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import DropDownMenu from '../components/DropDownMenu';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import {pickImage} from '../utils';
import APIRequest from '../network/APIRequest';
import ImagePicker from '../components/ImagePicker';
import ImageUpload from '../components/ImageUpload';
import CheckBox from '@react-native-community/checkbox';
import Loader from '../components/Loader';
import {useSelector} from 'react-redux';
import {isTextEmpty} from '../utils/index';

let imageFor = '';
function PostAds(props) {
  const viewPagerRef = React.createRef();
  const imagePickerRef = React.useRef(null);
  const userId = useSelector(state => state.auth.userId);
  const [selectPage, setSelectPage] = React.useState('');
  const [selectPagesList, setSelectPagesList] = React.useState([]);
  const [advertisementLocation, setAdvertisementLocation] = React.useState();
  const [advertisementLocationList, setAdvertisementList] = React.useState([]);
  const [startDate, setStartDate] = React.useState('');
  const [endDate, setEndDate] = React.useState('');
  const [title, setTitle] = React.useState('');
  const [isAcceptConditions, setIsAcceptConditions] = React.useState(false);
  const [fullName, setFullName] = React.useState('');
  const [age, setAge] = React.useState('');
  const [mobileNo, setMobileNo] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [companyName, setCompanyName] = React.useState('');
  const [businessType, setBusinessType] = React.useState('');
  const [gstNo, setGstNo] = React.useState('');
  const [address, setAddress] = React.useState('');
  const [pincode, setPincode] = React.useState('');
  const [state, setState] = React.useState('');
  const [city, setCity] = React.useState('');
  const [stateList, setStateList] = React.useState([]);
  const [cityList, setCityList] = React.useState([]);
  const [businessTypeList, setBusinessTypeList] = React.useState([]);
  const [country, setCountry] = React.useState('');
  const [isLoading, setLoading] = React.useState(false);
  const [description, setDescription] = React.useState('');
  const [userData, setUserData] = React.useState('');
  const [dateType, setDateType] = React.useState('');
  const [firstImage, setFirstImage] = React.useState(undefined);
  const [secondImage, setSecondImage] = React.useState(undefined);
  const [thirdImage, setThirdImage] = React.useState(undefined);
  const [fourthImage, setFourthImage] = React.useState(undefined);
  const [fifthImage, setFifthImage] = React.useState(undefined);
  const [sixthImage, setSixthImage] = React.useState(undefined);
  const [isDatePickerVisible, setDatePickerVisibility] = React.useState(false);

  useEffect(() => {
    getPagesList();
    getStateList();
    getBusinessTypeList();
    getAdvertisementLocationList();
    getCityList();
  }, []);

  function getPagesList() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/post_ads_page', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        const list = res.result
          .filter(item => {
            return item;
          })
          .map(({add_page_name, add_page_id}) => {
            return {
              name: add_page_name,
              id: add_page_id,
            };
          });
        setSelectPagesList(list);
      });
  }

  function getStateList() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/get_state', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        const list = res.state
          .filter(item => {
            return item;
          })
          .map(({state_name, state_id}) => {
            return {
              name: state_name,
              id: state_id,
            };
          });
        setStateList(list);
      });
  }

  function getCityList() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/post_ads_city', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        const list = res.result
          .filter(item => {
            return item;
          })
          .map(({city_name, city_id}) => {
            return {
              name: city_name,
              id: city_id,
            };
          });
        setCityList(list);
      });
  }

  function getAdvertisementLocationList() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    formdata.append('pageid', 24);
    console.log('formdata', formdata);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/post_ads_position',
      formdata,
      response => {
        if (response) {
          setLoading(false);
          const list = response.result
            .filter(item => {
              return item;
            })
            .map(({page_position, position_id}) => {
              return {
                name: page_position,
                id: position_id,
              };
            });
          setAdvertisementList(list);
        }
      },
    );
  }

  function getBusinessTypeList() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/get_category', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        const list = res.category
          .filter(item => {
            return item;
          })
          .map(({category_name, category_id}) => {
            return {
              name: category_name,
              id: category_id,
            };
          });
        setBusinessTypeList(list);
      });
  }

  const showDatePicker = type => {
    setDateType(type);
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = date => {
    var newDate = moment(date).format('DD-MM-YYYY');
    if (dateType === 'startDate') {
      setStartDate(newDate);
    } else {
      setEndDate(newDate);
    }
    hideDatePicker();
  };

  const changePage = pageIndex => {
    if (viewPagerRef.current) {
      viewPagerRef.current.setPage(pageIndex);
    }
  };

  const toggleImagePicker = () => {
    imagePickerRef.current?.open();
  };

  const chooseImage = type => {
    imageFor = type;
    toggleImagePicker();
  };

  const pick = type => {
    let pickerOptions = {
      cropping: true,
      cropperToolbarTitle: 'Scale and move with fingers to crop',
      hideBottomControls: true,
    };
    pickerOptions = {...pickerOptions, width: 640, height: 480};
    pickImage(type, pickerOptions)
      .then(handlePickedImage)
      .catch(error => {
        if (error && error.code === 'E_PERMISSION_MISSING') {
          Alert.alert('', 'Permission Missing');
        }
      });
  };

  const handlePickedImage = image => {
    let croppedImage;
    const parts = image.path.split('/');
    const fileName = parts[parts.length - 1];
    croppedImage = {
      uri: image.path,
      type: image.mime,
      name: fileName,
    };
    switch (imageFor) {
      case 'image1':
        setFirstImage(croppedImage);
        return;
      case 'image2':
        setSecondImage(croppedImage);
        return;
      case 'image3':
        setThirdImage(croppedImage);
        return;
      case 'image4':
        setFourthImage(croppedImage);
        return;
      case 'image5':
        setFifthImage(croppedImage);
        return;
      case 'image6':
        setSixthImage(croppedImage);
        return;
    }
  };

  function onSubmitData() {
    if (isTextEmpty(fullName)) {
      Alert.alert('', 'Please enter full name');
      return;
    }
    if (isTextEmpty(age)) {
      Alert.alert('', 'Please enter age');
      return;
    }
    if (isTextEmpty(mobileNo)) {
      Alert.alert('', 'Please enter mobile number');
      return;
    }
    if (isTextEmpty(email)) {
      Alert.alert('', 'Please enter email id');
      return;
    }
    if (isTextEmpty(companyName)) {
      Alert.alert('', 'Please enter company name');
      return;
    }
    if (!businessType) {
      Alert.alert('', 'Please select type of business');
      return;
    }
    if (isTextEmpty(gstNo)) {
      Alert.alert('', 'Please enter gst number');
      return;
    }
    if (isTextEmpty(address)) {
      Alert.alert('', 'Please enter address');
      return;
    }
    if (isTextEmpty(pincode)) {
      Alert.alert('', 'Please enter pincode');
      return;
    }
    if (isTextEmpty(country)) {
      Alert.alert('', 'Please enter country');
      return;
    }
    if (!state) {
      Alert.alert('', 'Please select state');
      return;
    }
    // changePage(3);
    setLoading(true);
    var formdata = new FormData();
    formdata.append('page_name', selectPage);
    formdata.append('position_id', advertisementLocation);
    formdata.append('city', city);
    formdata.append('userid', userId);
    formdata.append('ads_start_date', startDate);
    formdata.append('ads_end_date', endDate);
    formdata.append('ads_title', title);
    formdata.append('ads_description', description);
    formdata.append('full_name', fullName);
    formdata.append('age', age);
    formdata.append('mobile_no', mobileNo);
    formdata.append('email_id', email);
    formdata.append('ads_type', 'image');
    formdata.append('type_of_business', businessType);
    formdata.append('gst_number', gstNo);
    formdata.append('address1', address);
    formdata.append('address2', '');
    formdata.append('pin_code', pincode);
    formdata.append('state', state);
    formdata.append('amount', '25');
    formdata.append('country', country);
    formdata.append('company_name', companyName);
    console.log('formdata', formdata);
    var headers = {
      method: 'POST',
      body: formdata,
    };

    fetch('https://www.tradespine.co.in/json/post_ads_insert_value', headers)
      .then(response => response.json())
      .then(response => {
        console.log('response', response);
        if (response.order_id) {
          props.navigation.navigate('Webview', {
            orderId: response.order_id,
            userId: userId,
          });
        }
      });
  }

  function onConfirmOrder(orderId) {
    var data = new FormData();
    data.append('order_id', orderId);
    data.append('userid', userId);
    console.log('data', data);
    var request = {
      method: 'POST',
      body: data,
    };
    fetch('https://www.tradespine.co.in/json/confirm_order', request)
      .then(res => res.json())
      .then(res => {
        console.log('res confirmm', res.result[0]);
        setLoading(false);
        setUserData(res?.result[0]);
      });
  }

  function onSubmitStep1Data() {
    if (!selectPage) {
      Alert.alert('', 'Please select page');
      return;
    }
    if (!advertisementLocation) {
      Alert.alert('', 'Please select location for advertisement');
      return;
    }
    if (isTextEmpty(startDate)) {
      Alert.alert('', 'Please select start date');
      return;
    }
    if (isTextEmpty(endDate)) {
      Alert.alert('', 'Please select end date');
      return;
    }
    if (!city) {
      Alert.alert('', 'Please select city');
      return;
    }
    changePage(1);
  }

  function onSubmitStep2Data() {
    if (isTextEmpty(title)) {
      Alert.alert('', 'Please enter title');
      return;
    }
    if (isTextEmpty(description)) {
      Alert.alert('', 'Please enter description');
      return;
    }
    changePage(2);
  }

  const PostAdFirstStep = (
    <ScrollView style={{flex: 1}}>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 40}}>
          Post your Ads
        </Text>
        <View style={{flex: 1, paddingHorizontal: 10}}>
          <Text style={{fontSize: 20, marginTop: 40, marginLeft: 12}}>
            Step 1
          </Text>
          <DropDownMenu
            placeholderText={'Select Page'}
            options={selectPagesList}
            onSelectItem={item => {
              setSelectPage(item.id);
            }}
            style={{marginRight: 10}}
            inputStyle={{borderColor: '#d3cec4'}}
            icon={require('../assets/images/drop-down.png')}
          />
          <DropDownMenu
            placeholderText={'Location for advertisement'}
            options={advertisementLocationList}
            style={{marginRight: 10}}
            onSelectItem={item => {
              setAdvertisementLocation(item.id);
            }}
            icon={require('../assets/images/drop-down.png')}
            inputStyle={{borderColor: '#d3cec4'}}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderWidth: 1,
              marginTop: 25,
              borderColor: '#d3cec4',
              borderRadius: 5,
              fontSize: 16,
              width: '89%',
              height: 46,
              flex: 1,
              marginStart: 14,
            }}>
            <View style={{width: '80%'}}>
              <TextInput
                style={{
                  fontSize: 14,
                  paddingStart: 12,
                  color: 'black',
                }}
                onChangeText={setStartDate}
                value={startDate}
                editable={false}
                placeholder={'Ads start date'}
              />
            </View>
            <TouchableOpacity
              style={{width: '10%', marginStart: 10}}
              onPress={() => showDatePicker('startDate')}>
              <Image
                source={
                  props.icon
                    ? props.icon
                    : require('../assets/images/calendar.png')
                }
                style={[styles.ImageStyle]}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderWidth: 1,
              marginTop: 25,
              borderColor: '#d3cec4',
              borderRadius: 5,
              fontSize: 16,
              width: '89%',
              height: 46,
              flex: 1,
              marginStart: 14,
            }}>
            <View style={{width: '80%'}}>
              <TextInput
                style={{
                  fontSize: 14,
                  paddingStart: 12,
                  color: 'black',
                }}
                onChangeText={setEndDate}
                value={endDate}
                editable={false}
                placeholder={'Ads end date'}
              />
            </View>
            <TouchableOpacity
              style={{width: '10%', marginStart: 10}}
              onPress={() => showDatePicker('endDate')}>
              <Image
                source={
                  props.icon
                    ? props.icon
                    : require('../assets/images/calendar.png')
                }
                style={[styles.ImageStyle]}
              />
            </TouchableOpacity>
          </View>
          <DropDownMenu
            placeholderText={'Select city'}
            options={cityList}
            onSelectItem={item => {
              setCity(item.id);
            }}
            style={{marginRight: 10}}
            inputStyle={{borderColor: '#d3cec4'}}
            icon={require('../assets/images/drop-down.png')}
          />
        </View>
        <TouchableOpacity
          style={[styles.btn, {borderRadius: 8}]}
          onPress={() => onSubmitStep1Data()}>
          <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
      <ImagePicker
        ref={imagePickerRef}
        onCameraSelect={() => pick('camera')}
        onLibrarySelect={() => pick('library')}
      />
    </ScrollView>
  );

  const PostAdSecondStep = (
    <ScrollView style={{flex: 1}}>
      <View style={{flex: 1, marginHorizontal: 10}}>
        <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 40}}>
          Fill Ads Details
        </Text>
        <TextInput
          style={[styles.input, {marginTop: 30}]}
          onChangeText={setTitle}
          value={title}
          placeholder={'Title'}
        />
        <Text style={{fontSize: 16, color: '#b3b0ae'}}>
          Mention the key features of your ad
        </Text>
        <TextInput
          style={[styles.input, {marginTop: 30, height: 100}]}
          onChangeText={setDescription}
          value={description}
          placeholder={'Description'}
        />
        <Text style={{fontSize: 16, color: '#b3b0ae'}}>
          Include all the details of ad and key features
        </Text>
        <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 30}}>
          Upload images
        </Text>

        <View style={{flexDirection: 'row'}}>
          <ImageUpload
            style={{marginTop: 12}}
            onPress={() => chooseImage('image1')}
            selectedImage={firstImage}
          />
          <ImageUpload
            style={{marginTop: 12, marginLeft: 10}}
            onPress={() => chooseImage('image2')}
            selectedImage={secondImage}
          />
          <ImageUpload
            style={{marginTop: 12, marginLeft: 10}}
            onPress={() => chooseImage('image3')}
            selectedImage={thirdImage}
          />
        </View>
        <View style={{flexDirection: 'row'}}>
          <ImageUpload
            style={{marginTop: 12}}
            onPress={() => chooseImage('image4')}
            selectedImage={fourthImage}
          />
          <ImageUpload
            photoLabel={'test'}
            style={{marginTop: 12, marginLeft: 10}}
            onPress={() => chooseImage('image5')}
            selectedImage={fifthImage}
          />
          <ImageUpload
            photoLabel={'test'}
            style={{marginTop: 12, marginLeft: 10}}
            onPress={() => chooseImage('image6')}
            selectedImage={sixthImage}
          />
        </View>
        <TouchableOpacity
          style={[styles.btn, {width: '100%', borderRadius: 8}]}
          onPress={() => onSubmitStep2Data()}>
          <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );

  const PostAdThirdStep = (
    <ScrollView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <View style={{marginHorizontal: 10}}>
          <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 40}}>
            Business Details
          </Text>
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setFullName}
            value={fullName}
            placeholder={'Full Name'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setAge}
            value={age}
            placeholder={'Age'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setMobileNo}
            value={mobileNo}
            placeholder={'Mobile Number'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setEmail}
            value={email}
            placeholder={'Email Id'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setCompanyName}
            value={companyName}
            placeholder={'Company Name/Shop Name'}
          />
        </View>
        <DropDownMenu
          placeholderText={'Type of Business'}
          options={businessTypeList}
          parentStyle={{marginHorizontal: 10}}
          style={{width: '100%'}}
          onSelectItem={item => {
            setBusinessType(item.id);
          }}
          inputStyle={{borderColor: '#d3cec4'}}
          icon={require('../assets/images/drop-down.png')}
        />
        <View style={{marginHorizontal: 10}}>
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setGstNo}
            value={gstNo}
            placeholder={'Gst Number'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setAddress}
            value={address}
            placeholder={'Address'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setPincode}
            value={pincode}
            placeholder={'Pincode'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setCountry}
            value={country}
            placeholder={'Country'}
          />
        </View>
        <DropDownMenu
          placeholderText={'Select state'}
          options={stateList}
          parentStyle={{marginHorizontal: 10}}
          style={{width: '100%'}}
          onSelectItem={item => {
            setState(item.name);
          }}
          inputStyle={{borderColor: '#d3cec4'}}
          icon={require('../assets/images/drop-down.png')}
        />
        <View style={{marginHorizontal: 10}}>
          <TouchableOpacity
            style={[styles.btn, {width: '100%', borderRadius: 8}]}
            onPress={() => onSubmitData()}>
            <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
              Next
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );

  const PostAdFourthStep = (
    <ScrollView style={{flex: 1}}>
      <View style={{marginHorizontal: 10, backgroundColor: '#FFFF'}}>
        <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 40}}>
          Amount Payable
        </Text>
        <View
          style={{
            marginTop: 40,
            marginHorizontal: 20,
            alignItems: 'center',
            backgroundColor: '#fdf0f1',
            padding: 16,
            borderRadius: 8,
          }}>
          <Text style={{fontSize: 20, marginTop: 10, fontWeight: 'bold'}}>
            India pvt ltd
          </Text>
          <Text style={{fontSize: 20, marginTop: 10, fontWeight: 'bold'}}>
            Home Page
          </Text>
          <Text style={{fontSize: 20, marginTop: 10, fontWeight: 'bold'}}>
            Page Second
          </Text>
          <Text style={{fontSize: 18, marginVertical: 10}}>
            21 feb to 22 april
          </Text>
        </View>
        <View
          style={{
            marginTop: 30,
            marginHorizontal: 20,
            alignItems: 'center',
            backgroundColor: '#fdf0f1',
            padding: 16,
            borderRadius: 8,
          }}>
          <Text style={{fontSize: 20, marginTop: 10}}>Amount 999</Text>
          <Text style={{fontSize: 20, marginTop: 10}}>Discount 50%</Text>
          <Text style={{fontSize: 20, marginTop: 10}}>Tax 90%</Text>
        </View>
        <View
          style={{
            marginTop: 30,
            marginHorizontal: 20,
            alignItems: 'center',
            backgroundColor: '#fdf0f1',
            padding: 16,
            borderRadius: 8,
          }}>
          <Text style={{fontSize: 20, marginTop: 10}}>Total Amount</Text>
          <Text style={{fontSize: 24, marginTop: 10}}>499 per/month</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 20,
          }}>
          <CheckBox
            disabled={false}
            value={isAcceptConditions}
            onValueChange={newValue => setIsAcceptConditions(newValue)}
          />
          <Text style={{fontSize: 14, color: '#393939', marginTop: 5}}>
            I accept terms and conditions.
          </Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TouchableOpacity
            onPress={() => changePage(2)}
            style={[
              styles.btn,
              {
                width: '45%',
                backgroundColor: '#FFFF',
                borderColor: '#BA2525',
                borderRadius: 8,
                borderWidth: 1,
              },
            ]}>
            <Text style={{fontSize: 18, color: '#BA2525', fontWeight: 'bold'}}>
              Preview
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.btn, {width: '45%', borderRadius: 8}]}>
            <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
              Pay Now
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <ViewPager
        style={{flex: 1}}
        initialPage={0}
        scrollEnabled={false}
        transitionStyle="scroll"
        showPageIndicator={false}
        ref={viewPagerRef}>
        {PostAdFirstStep}
        {PostAdSecondStep}
        {PostAdThirdStep}
        {/* {PostAdFourthStep} */}
      </ViewPager>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 5,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderColor: '#d3cec4',
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 46,
    color: 'black',
  },
  ImageStyle: {
    height: 20,
    width: 20,
    margin: 12,
    tintColor: '#C0C0C0',
  },
  btn: {
    alignItems: 'center',
    padding: 10,
    marginTop: 40,
    width: '90%',
    height: 44,
    backgroundColor: '#BA2525',
    marginBottom: 20,
  },
});

export default PostAds;
