/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
} from 'react-native';
import {isTextEmpty} from '../utils/index';
import APIRequest from '../network/APIRequest';
import CheckBox from '@react-native-community/checkbox';
import {getUniqueId} from 'react-native-device-info';
import DropDownMenu from '../components/DropDownMenu';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import Loader from '../components/Loader';

function Signup(props) {
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [age, setAge] = React.useState();
  const [dob, setDob] = React.useState('');
  const [address, setAddress] = React.useState('');
  const [city, setCity] = React.useState('');
  const [state, setState] = React.useState('');
  const [country, setCountry] = React.useState('');
  const [pincode, setPincode] = React.useState('');
  const [businessType, setBusinessType] = React.useState('');
  const [gstNumber, setGstNumber] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [mobileNo, setMobileNo] = React.useState('');
  const [userRole, setUserRole] = React.useState('');
  const [userName, setUserName] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [interestedFields, setInterestedFields] = React.useState('');
  const [bottomSheetType, setBottomSheetType] = React.useState('');
  const [businessInterests, setBusinessInterests] = React.useState('');
  const [bottomSheetOptions, setBottomSheetOptions] = React.useState([
    'User',
    'Retailer',
  ]);
  const [categoryList, setCategoryList] = React.useState([]);
  const [isSiteNotification, setIsSiteNotification] = React.useState(false);
  const [isAcceptConditions, setIsAcceptConditions] = React.useState(false);
  const [stateList, setStateList] = React.useState([]);
  const [cityList, setCityList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(false);
  const [userRoleList, setUserRoleList] = React.useState([]);
  const [isDatePickerVisible, setDatePickerVisibility] = React.useState(false);
  console.log('userRole', userRole);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  function getAge(birthYear) {
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    const userAge = currentYear - birthYear;
    return userAge;
  }

  const handleConfirm = date => {
    var newDate = moment(date).format('DD-MM-YYYY');
    setDob(newDate);
    hideDatePicker();
    const year = moment(date).format('YYYY');
    if (year) {
      setAge(`${getAge(year)}`);
    }
  };

  useEffect(() => {
    getUserRole();
    getCategoryList();
    getStates();
    getCities();
  }, []);

  function getUserRole() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/get_user_type', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        const list = res.user_type
          .filter(item => {
            return item;
          })
          .map(({user_type_name, user_type_id}) => {
            return {
              name: user_type_name,
              id: user_type_id,
            };
          });
        setUserRoleList(list);
      });
  }

  function getCategoryList() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/get_category', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        const list = res.category
          .filter(item => {
            return item;
          })
          .map(({category_name, category_id}) => {
            return {
              name: category_name,
              id: category_id,
            };
          });
        setCategoryList(list);
      });
  }
  function getCities() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/get_city', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        const list = res.city
          .filter(item => {
            return item;
          })
          .map(({city_name, city_id}) => {
            return {
              name: city_name,
              id: city_id,
            };
          });
        setCityList(list);
      });
  }
  function getStates() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/get_state', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        const list = res.state
          .filter(item => {
            return item;
          })
          .map(({state_name, state_id}) => {
            return {
              name: state_name,
              id: state_id,
            };
          });
        setStateList(list);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <TouchableOpacity
        style={{
          marginHorizontal: 16,
          marginTop: 30,
          padding: 5,
        }}
        onPress={() => props.navigation.goBack()}>
        <Image
          source={require('../assets/images/leftArrow.png')}
          style={{height: 24, width: 24}}
        />
      </TouchableOpacity>
      <ScrollView>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            margin: 12,
          }}>
          <Text
            style={{
              fontSize: 26,
              fontWeight: 'bold',
              color: '#F68592',
              marginTop: 40,
            }}>
            Register
          </Text>
          <DropDownMenu
            placeholderText={'Select you are'}
            options={userRoleList}
            onSelectItem={item => {
              setUserRole(item.id);
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              justifyContent: 'space-between',
            }}>
            <TextInput
              style={[styles.input, {width: '48%', marginEnd: 5}]}
              onChangeText={setFirstName}
              value={firstName}
              placeholder={'First Name'}
            />
            <TextInput
              style={[styles.input, {width: '48%', marginStart: 5}]}
              onChangeText={setLastName}
              value={lastName}
              placeholder={'Last Name'}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <TextInput
              style={[styles.input, {width: '48%', marginEnd: 5}]}
              onChangeText={setAge}
              value={age}
              placeholder={'Age'}
            />
            <TouchableOpacity style={{width: '48%'}} onPress={showDatePicker}>
              <TextInput
                style={[styles.input, {marginStart: 5}]}
                onChangeText={setDob}
                value={dob}
                placeholder={'Date of birth'}
                editable={false}
              />
            </TouchableOpacity>
          </View>
          <DropDownMenu
            placeholderText={'City'}
            options={cityList}
            onSelectItem={item => {
              setCity(item.id);
            }}
          />
          <DropDownMenu
            placeholderText={'Select state'}
            options={stateList}
            onSelectItem={item => {
              setState(item.id);
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <TextInput
              style={[styles.input, {width: '48%', marginEnd: 5}]}
              onChangeText={setAddress}
              value={address}
              placeholder={'Address'}
            />
            <TextInput
              style={[styles.input, {width: '48%', marginStart: 5}]}
              onChangeText={setCountry}
              value={country}
              placeholder={'Country'}
            />
          </View>
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setPincode}
            value={pincode}
            placeholder={'Pin Code'}
          />
          <DropDownMenu
            placeholderText={'Interested Fields'}
            options={categoryList}
            onSelectItem={item => {
              setInterestedFields(item.id);
            }}
          />
          <DropDownMenu
            placeholderText={'Interested in business'}
            options={categoryList}
            onSelectItem={item => {
              setBusinessInterests(item.id);
            }}
          />
          {userRole !== '1' && (
            <TextInput
              style={[styles.input, {marginTop: 30}]}
              onChangeText={setGstNumber}
              value={gstNumber}
              placeholder={'GST Number'}
            />
          )}
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <TextInput
              style={[styles.input, {width: '48%', marginEnd: 5}]}
              onChangeText={setEmail}
              value={email}
              placeholder={'Email Id'}
            />
            <TextInput
              style={[styles.input, {width: '48%', marginStart: 5}]}
              onChangeText={setMobileNo}
              value={mobileNo}
              placeholder={'Mobile Number'}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              justifyContent: 'space-between',
            }}>
            <TextInput
              style={[styles.input, {width: '48%', marginEnd: 5}]}
              onChangeText={setUserName}
              value={userName}
              placeholder={'Username'}
            />
            <TextInput
              style={[styles.input, {width: '48%', marginStart: 5}]}
              onChangeText={setPassword}
              value={password}
              placeholder={'Password'}
            />
          </View>
          <View style={{width: '100%'}}>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 30,
              }}>
              <CheckBox
                disabled={false}
                value={isSiteNotification}
                onValueChange={newValue => setIsSiteNotification(newValue)}
              />
              <Text style={{fontSize: 14, color: '#393939', marginTop: 5}}>
                Allow to site Notification
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
              }}>
              <CheckBox
                disabled={false}
                value={isAcceptConditions}
                onValueChange={newValue => setIsAcceptConditions(newValue)}
              />
              <Text style={{fontSize: 14, color: '#393939', marginTop: 5}}>
                I accept terms and conditions.
              </Text>
            </View>
          </View>
          <TouchableOpacity style={styles.button} onPress={onRegister}>
            <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
              SIGN UP
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </SafeAreaView>
  );

  function onRegister() {
    if (isTextEmpty(userRole)) {
      Alert.alert('', 'Please enter user role');
      return;
    }
    if (isTextEmpty(firstName)) {
      Alert.alert('', 'Please enter first name');
      return;
    }
    if (isTextEmpty(lastName)) {
      Alert.alert('', 'Please enter last name');
      return;
    }
    if (isTextEmpty(age)) {
      Alert.alert('', 'Please enter age');
      return;
    }
    if (isTextEmpty(dob)) {
      Alert.alert('', 'Please enter date of birth');
      return;
    }
    if (isTextEmpty(address)) {
      Alert.alert('', 'Please enter address');
      return;
    }
    if (isTextEmpty(city)) {
      Alert.alert('', 'Please enter city');
      return;
    }
    if (isTextEmpty(state)) {
      Alert.alert('', 'Please enter state');
      return;
    }
    if (isTextEmpty(country)) {
      Alert.alert('', 'Please enter country');
      return;
    }
    if (isTextEmpty(pincode)) {
      Alert.alert('', 'Please enter pin code');
      return;
    }
    if (isTextEmpty(interestedFields)) {
      Alert.alert('', 'Please enter interested fields');
      return;
    }
    if (isTextEmpty(businessInterests)) {
      Alert.alert('', 'Please enter business interests');
      return;
    }
    if (isTextEmpty(email)) {
      Alert.alert('', 'Please enter email');
      return;
    }
    if (isTextEmpty(mobileNo)) {
      Alert.alert('', 'Please enter mobile number');
      return;
    }
    if (isTextEmpty(userName)) {
      Alert.alert('', 'Please enter username');
      return;
    }
    if (isTextEmpty(password)) {
      Alert.alert('', 'Please enter password');
      return;
    }
    if (!isSiteNotification) {
      Alert.alert('', 'Please allow site notification');
      return;
    }
    if (!isAcceptConditions) {
      Alert.alert('', 'Please accept terms and conditions');
      return;
    }
    setLoading(true);
    let formdata = new FormData();
    formdata.append('first_name', firstName);
    formdata.append('last_name ', lastName);
    formdata.append('age', age);
    formdata.append('date_of_birth ', dob);
    formdata.append('user_type', userRole);
    formdata.append('business_adress ', address);
    formdata.append('business_type ', businessInterests);
    formdata.append('intersted_field ', interestedFields);
    formdata.append('site_notification ', isSiteNotification);
    formdata.append('gst_number ', gstNumber);
    formdata.append('city ', city);
    formdata.append('state ', state);
    formdata.append('country ', country);
    formdata.append('pin ', pincode);
    formdata.append('email', email);
    formdata.append('mobile', mobileNo);
    formdata.append('username', userName);
    formdata.append('password', password);
    formdata.append('ipaddress', getUniqueId());
    console.log('formdata', formdata);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/signup',
      formdata,
      response => {
        if (response) {
          setLoading(false);
          if (response?.user_id) {
            Alert.alert(
              '',
              'Registered sucessfully, Please activate account to login',
              [{text: 'OK', onPress: () => props.navigation.navigate('Login')}],
            );
          }
        }
      },
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderColor: '#F68592',
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 46,
    color: 'black',
  },
  button: {
    alignItems: 'center',
    padding: 10,
    marginTop: 20,
    borderRadius: 8,
    width: '100%',
    height: 44,
    backgroundColor: '#BA2525',
    marginBottom: 20,
  },
  ImageStyle: {
    height: 20,
    width: 20,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 12,
    tintColor: '#C0C0C0',
  },
});

export default Signup;
