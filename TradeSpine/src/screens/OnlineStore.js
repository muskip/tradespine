/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  FlatList,
  ScrollView,
  Dimensions,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import APIRequest from '../network/APIRequest';
import {CommonConstants} from '../constants/index';
import Loader from '../components/Loader';
import {FlatListSlider} from 'react-native-flatlist-slider';
import {ImagePreview} from '../components/ImagePreview';
import AdsImage from '../components/AdsImage';
import {getAdsIndex} from '../utils/index';

function OnlineStore(props) {
  const userId = useSelector(state => state.auth.userId);
  const [onlineStores, setOnlineStores] = React.useState([]);
  const [topHeaderItemsList, setTopHeaderItemsList] = React.useState([]);
  const [onlinestoreAdsList, setOnlinestoreAdsList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(false);
  const [adsCurrentIndex, setAdsCurrentIndex] = React.useState(null);
  const width = Dimensions.get('window').width;
  let tempList = [];
  let sum = 0;
  const isShowMoreAds = adsCurrentIndex
    ? onlinestoreAdsList.length > adsCurrentIndex + 1
    : onlinestoreAdsList.length
    ? true
    : false;
  // console.log('isShowMoreAds', isShowMoreAds);
  // console.log('onlinestoreAdsList', onlinestoreAdsList);

  useEffect(() => {
    getTopHeaderData();
    getOnlineStores();
    getOnlinestoreAds();
  }, []);

  function getOnlinestoreAds() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/all_online_store_ads',
      formdata,
      response => {
        if (response) {
          console.log('online adsss--->>>>>', response);
          setLoading(false);
          setOnlinestoreAdsList(response?.result);
        }
      },
    );
  }

  function getTopHeaderData() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/top_ads_all_online_store',
      formdata,
      response => {
        if (response) {
          console.log('response top_ads_wholeseller', response);
          setLoading(false);
          setTopHeaderItemsList(response?.result);
        }
      },
    );
  }

  function getOnlineStores() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/all_online_store',
      formdata,
      response => {
        if (response) {
          console.log('response online store', response);
          setLoading(false);
          setOnlineStores(response?.result);
        }
      },
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <ScrollView>
        {topHeaderItemsList && topHeaderItemsList.length > 0 && (
          <View style={{flex: 1}}>
            <View>
              <FlatListSlider
                data={topHeaderItemsList}
                timer={5000}
                component={<ImagePreview containerWidth={width - 10} />}
                indicator={false}
                contentContainerStyle={{paddingHorizontal: 5}}
              />
            </View>
          </View>
        )}
        {onlineStores && onlineStores.length > 0 && (
          <View style={{flex: 1}}>
            <FlatList
              data={onlineStores}
              contentContainerStyle={{
                marginBottom: 20,
                marginHorizontal: 10,
                padding: 10,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item, index}) => {
                let adsIndex = null;
                const i = index > 0 ? 3 : 2;
                sum = sum + i;
                tempList.push(sum);
                const isAdsIndex = tempList.includes(index);
                if (isAdsIndex) {
                  adsIndex = getAdsIndex(index);
                  if (onlinestoreAdsList[adsIndex]) {
                    setAdsCurrentIndex(adsIndex);
                  }
                }
                return isAdsIndex && onlinestoreAdsList[adsIndex] ? (
                  <AdsImage item={onlinestoreAdsList[adsIndex]} />
                ) : (
                  <View
                    style={[
                      styles.shadow,
                      {
                        flex: 1,
                        flexDirection: 'column',
                        margin: 1,
                        backgroundColor: '#FFFF',
                        marginHorizontal: 5,
                        marginTop: 10,
                        padding: 10,
                      },
                    ]}>
                    <Image
                      resizeMode={'contain'}
                      style={[
                        styles.imageThumbnail,
                        {
                          width: '100%',
                          height: 150,
                          backgroundColor: '#f6f2ef',
                        },
                      ]}
                      source={{
                        uri: item.store_image_name
                          ? `${CommonConstants.IMAGE_URL}/images/store_images/${item.store_image_name}`
                          : `${CommonConstants.IMAGE_URL}/images/sellers.jpg`,
                      }}
                    />
                    <View style={{marginHorizontal: 10, marginTop: 20}}>
                      <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                        {`${item?.store_name?.toUpperCase() ?? ''}`}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginStart: 2,
                          color: '#b8b1a6',
                          marginTop: 10,
                        }}>
                        {`${item?.email ?? ''}`}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginStart: 2,
                          color: '#b8b1a6',
                          marginTop: 10,
                        }}>
                        {`${item?.mobile_no ?? ''}`}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginStart: 2,
                          color: '#b8b1a6',
                          marginTop: 10,
                        }}>
                        {`${item?.address_line1 ?? ''} ${
                          item?.address_line2 ?? ''
                        }`}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginTop: 10,
                          color: '#b8b1a6',
                        }}>
                        {'Member since 11 days'}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginVertical: 14,
                          color: '#b8b1a6',
                        }}>
                        {`${item?.description ?? ''}`}
                      </Text>
                    </View>
                  </View>
                );
              }}
            />
          </View>
        )}
        <View
          style={{
            marginBottom: 20,
            paddingHorizontal: 16,
            paddingBottom: 10,
            marginHorizontal:12
          }}>
          {isShowMoreAds &&
            onlinestoreAdsList &&
            onlinestoreAdsList.length &&
            onlinestoreAdsList.map((item, index) => {
              console.log('item', item);
              if (adsCurrentIndex) {
                if (index > adsCurrentIndex) {
                  return <AdsImage item={onlinestoreAdsList[index]} />;
                }
              } else {
                return <AdsImage item={onlinestoreAdsList[index]} />;
              }
            })}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 80,
    color: 'black',
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default OnlineStore;
