/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Dimensions,
  ScrollView,
  Text,
  FlatList,
  Image,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import APIRequest from '../network/APIRequest';
import {FlatListSlider} from 'react-native-flatlist-slider';
import {ImagePreview} from '../components/ImagePreview';
import Loader from '../components/Loader';
import {CommonConstants} from '../constants/index';
import {TouchableOpacity} from 'react-native-gesture-handler';

function AfterLogin(props) {
  const userId = useSelector(state => state.auth.userId);
  const [topHeaderItemsList, setTopHeaderItemsList] = React.useState([]);
  const [topRankingProductList, setTopRankingProductList] = React.useState([]);
  const [weekelyEventsList, setWeekelyEventsList] = React.useState([]);
  const [onlineStores, setOnlineStores] = React.useState([]);
  const [wholeSellers, setWholeSellers] = React.useState([]);
  const [offersList, setOffers] = React.useState([]);
  const [articalsList, setArticals] = React.useState([]);
  const [newOnlineStores, setNewOnlineStores] = React.useState([]);
  const [brandAdsList, setBrandAdsList] = React.useState([]);
  const [retailerList, setRetailerList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(false);
  const [showMoreProducts, setShowMoreProducts] = React.useState(false);
  const [showAllProducts, setShowAllProducts] = React.useState(false);
  const width = Dimensions.get('window').width;

  useEffect(() => {
    getTopHeaderData();
    getOnlineStores();
    getTopRankingProductList();
    getWholeSellerData();
    getRetailerData();
    getNewOnlineStores();
    getWeekelyEventsList();
    getOffers();
    getArticals();
    getBrandAds();
  }, []);

  function getTopHeaderData() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/dashboard_header_slider',
      formdata,
      response => {
        if (response) {
          // console.log('response dashboard header slider', response);
          setLoading(false);
          setTopHeaderItemsList(response?.result);
        }
      },
    );
  }

  function getRetailerData() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/get_top_retailer',
      formdata,
      response => {
        if (response) {
          // console.log('response retailer', response);
          setRetailerList(response?.result);
          setLoading(false);
        }
      },
    );
  }

  function getTopRankingProductList() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/top_ranking_product',
      formdata,
      response => {
        if (response) {
          // console.log('response top ranking product', response);
          setLoading(false);
          const list = response?.result;
          console.log('list', list.slice(0, 3));
          setShowMoreProducts(list.length > 3);
          setShowAllProducts(list);
          setTopRankingProductList(list.length > 3 ? list.slice(0, 3) : list);
        }
      },
    );
  }

  function getWeekelyEventsList() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/weekly_event',
      formdata,
      response => {
        if (response) {
          // console.log('weekly_event', response?.result);
          setLoading(false);
          setWeekelyEventsList(response?.result);
        }
      },
    );
  }

  function getArticals() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/after_login_get_articles',
      formdata,
      response => {
        if (response) {
          // console.log('response top ranking product', response);
          setLoading(false);
          setArticals(response?.result);
        }
      },
    );
  }

  function getWholeSellerData() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/get_top_wholeseller',
      formdata,
      response => {
        if (response) {
          console.log('response WholeSeller', response);
          setLoading(false);
          setWholeSellers(response?.result);
        }
      },
    );
  }

  function getOnlineStores() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/dashboard_online_store',
      formdata,
      response => {
        if (response) {
          console.log('response online store', response);
          setLoading(false);
          setOnlineStores(response?.result);
        }
      },
    );
  }

  function getOffers() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/after_login_get_latest_offer',
      formdata,
      response => {
        if (response) {
          // console.log('response offers', response);
          setLoading(false);
          setOffers(response?.result);
        }
      },
    );
  }

  function getNewOnlineStores() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/after_login_new_online_store',
      formdata,
      response => {
        if (response) {
          // console.log('response new_online_store', response);
          setLoading(false);
          setNewOnlineStores(response?.result);
        }
      },
    );
  }

  function getBrandAds() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/after_login_brand_ads',
      formdata,
      response => {
        if (response) {
          // console.log('response new_online_store', response);
          setLoading(false);
          setBrandAdsList(response?.result);
        }
      },
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <ScrollView>
        {topHeaderItemsList && topHeaderItemsList.length > 0 && (
          <View style={{flex: 1}}>
            <View>
              <FlatListSlider
                data={topHeaderItemsList}
                timer={5000}
                component={<ImagePreview containerWidth={width - 10} />}
                indicator={false}
                contentContainerStyle={{paddingHorizontal: 5}}
              />
            </View>
          </View>
        )}
        {onlineStores && onlineStores.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 10,
                textAlign: 'center',
              }}>
              ONLINE STORE
            </Text>
            <FlatList
              data={onlineStores}
              contentContainerStyle={{
                marginTop: 20,
                padding: 16,
                backgroundColor: '#FDE3E6',
              }}
              horizontal={true}
              keyExtractor={(item, index) => index}
              renderItem={({item}) => (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      width: width - 160,
                      backgroundColor: '#FFFF',
                      height: width / 2,
                      borderRadius: 10,
                      marginHorizontal: 5,
                    },
                  ]}>
                  <Image
                    resizeMode={'contain'}
                    style={{width: width - 20, height: 160}}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/images/product_image/${item.product_image_name}`,
                    }}
                  />
                </View>
              )}
            />
          </View>
        )}

        {topRankingProductList && topRankingProductList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 20,
                textAlign: 'center',
              }}>
              TOP RANKING PRODUCTS
            </Text>
            <FlatList
              data={showMoreProducts ? topRankingProductList : showAllProducts}
              contentContainerStyle={{
                marginTop: 20,
                backgroundColor: '#FDE3E6',
                marginHorizontal: 10,
                borderRadius: 10,
                padding: 10,
              }}
              numColumns={2}
              keyExtractor={(item, index) => index}
              renderItem={({item, index}) => (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      flex: 1,
                      flexDirection: 'column',
                      margin: 1,
                      backgroundColor: '#FFFF',
                      borderRadius: 10,
                      marginHorizontal: 5,
                      marginTop: 10,
                      padding: 10,
                    },
                  ]}>
                  <Image
                    resizeMode={'contain'}
                    style={[
                      styles.imageThumbnail,
                      {
                        width: '100%',
                        height: 150,
                      },
                    ]}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/images/product_image/${item.product_image_name}`,
                    }}
                  />
                </View>
              )}
            />
            {showMoreProducts && (
              <TouchableOpacity
                style={{
                  paddingRight: 10,
                  backgroundColor: '#f4717e',
                  marginBottom: 10,
                  height: 46,
                  width: 46,
                  alignSelf: 'center',
                  marginRight: 5,
                  borderRadius: 46 / 2,
                }}
                onPress={() => setShowMoreProducts(false)}>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                  }}>
                  <Image
                    source={require('../assets/images/downArrow.png')}
                    style={{
                      width: 20,
                      height: 20,
                      tintColor: '#FFFF',
                      left: 5,
                    }}
                  />
                </View>
              </TouchableOpacity>
            )}
          </View>
        )}

        {articalsList && articalsList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 30,
                textAlign: 'center',
              }}>
              MOST BRAND COMPANY
            </Text>
            <FlatList
              data={onlineStores}
              contentContainerStyle={{
                marginTop: 20,
                padding: 16,
                backgroundColor: '#edafaf',
              }}
              horizontal={true}
              keyExtractor={(item, index) => index}
              renderItem={({item}) => (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      width: width - 160,
                      backgroundColor: '#FFFF',
                      height: width / 2,
                      marginHorizontal: 5,
                    },
                  ]}>
                  <Image
                    resizeMode={'contain'}
                    style={{width: width - 170, height: 160}}
                    source={require('../assets/images/thumbnail1.png')}
                  />
                </View>
              )}
            />
          </View>
        )}

        {wholeSellers && wholeSellers.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 30,
                textAlign: 'center',
              }}>
              BEST WHOLE-SELLER
            </Text>
            <FlatList
              data={wholeSellers}
              contentContainerStyle={{
                marginTop: 20,
                backgroundColor: '#FDE3E6',
                padding: 16,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item}) => (
                <View
                  style={[
                    styles.shadow,
                    {
                      width: width - 20,
                      backgroundColor: '#FFFF',
                      borderRadius: 10,
                      marginHorizontal: 8,
                      flexDirection: 'row',
                      right: 16,
                      flex: 1,
                      paddingHorizontal: 5,
                      paddingVertical: 10,
                    },
                  ]}>
                  <View style={{flex: 0.5}}>
                    <Image
                      resizeMode={'contain'}
                      style={{
                        width: width - 220,
                        height: 160,
                      }}
                      source={{
                        uri: `${CommonConstants.IMAGE_URL}/images/store_images/${item.store_image_name}`,
                      }}
                    />
                  </View>
                  <View style={{flex: 0.6}}>
                    {(item.first_name || item.last_name) && (
                      <Text
                        style={{
                          fontSize: 18,
                          fontWeight: 'bold',
                        }}>
                        {`${item.first_name.toUpperCase()} ${item.last_name.toUpperCase()}`}
                      </Text>
                    )}
                    {item.business_adress && (
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Image
                          source={require('../assets/images/location.png')}
                          style={{
                            width: 16,
                            height: 16,
                            alignSelf: 'center',
                            tintColor: '#a9a499',
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 14,
                            marginStart: 2,
                            color: '#b8b1a6',
                          }}>
                          {`${item.business_adress} ${item.city_name}`}
                        </Text>
                      </View>
                    )}
                    <Text
                      style={{
                        fontSize: 18,
                        marginTop: 20,
                        color: '#726e74',
                      }}>
                      {'Member since 11 days'}
                    </Text>
                    <View
                      style={{
                        marginTop: 20,
                        paddingRight: 10,
                        backgroundColor: '#d64545',
                        height: 32,
                        width: 32,
                        alignSelf: 'flex-end',
                        marginRight: 5,
                        borderRadius: 32 / 2,
                      }}>
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          flex: 1,
                        }}>
                        <Image
                          source={require('../assets/images/like.png')}
                          style={{
                            width: 16,
                            height: 16,
                            tintColor: '#FFFF',
                            left: 5,
                          }}
                        />
                        <View
                          style={{
                            position: 'absolute',
                            height: 14,
                            width: 14,
                            borderRadius: 14 / 2,
                            backgroundColor: '#95c943',
                            top: 0,
                            right: 0,
                            left: 24,
                          }}>
                          <Text
                            style={{
                              fontSize: 8,
                              textAlign: 'center',
                              fontWeight: 'bold',
                              color: '#FFFF',
                              top: 1,
                            }}>
                            0
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />
          </View>
        )}

        {retailerList && retailerList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 30,
                textAlign: 'center',
              }}>
              BEST RETAILER
            </Text>
            <FlatList
              data={retailerList}
              contentContainerStyle={{
                marginTop: 20,
                backgroundColor: '#FDE3E6',
                padding: 16,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item}) => (
                <View
                  style={[
                    styles.shadow,
                    {
                      width: width - 20,
                      backgroundColor: '#FFFF',
                      borderRadius: 10,
                      marginHorizontal: 8,
                      flexDirection: 'row',
                      right: 16,
                      flex: 1,
                      paddingHorizontal: 5,
                      paddingVertical: 10,
                    },
                  ]}>
                  <View style={{flex: 0.5}}>
                    <Image
                      resizeMode={'contain'}
                      style={{
                        width: width - 220,
                        height: 160,
                      }}
                      source={{
                        uri: `${CommonConstants.IMAGE_URL}/images/user_images/${item.user_images}`,
                      }}
                    />
                  </View>
                  <View style={{flex: 0.6}}>
                    {(item.first_name || item.last_name) && (
                      <Text
                        style={{
                          fontSize: 18,
                          fontWeight: 'bold',
                        }}>
                        {`${item.first_name.toUpperCase()} ${item.last_name.toUpperCase()}`}
                      </Text>
                    )}
                    {item.business_adress && (
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Image
                          source={require('../assets/images/location.png')}
                          style={{
                            width: 16,
                            height: 16,
                            alignSelf: 'center',
                            tintColor: '#a9a499',
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 14,
                            marginStart: 2,
                            color: '#b8b1a6',
                          }}>
                          {`${item.business_adress} ${item.city_name}`}
                        </Text>
                      </View>
                    )}
                    <Text
                      style={{
                        fontSize: 18,
                        marginTop: 20,
                        color: '#726e74',
                      }}>
                      {'Member since 11 days'}
                    </Text>
                    <View
                      style={{
                        marginTop: 20,
                        paddingRight: 10,
                        backgroundColor: '#d64545',
                        height: 32,
                        width: 32,
                        alignSelf: 'flex-end',
                        marginRight: 5,
                        borderRadius: 32 / 2,
                      }}>
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          flex: 1,
                        }}>
                        <Image
                          source={require('../assets/images/like.png')}
                          style={{
                            width: 16,
                            height: 16,
                            tintColor: '#FFFF',
                            left: 5,
                          }}
                        />
                        <View
                          style={{
                            position: 'absolute',
                            height: 14,
                            width: 14,
                            borderRadius: 14 / 2,
                            backgroundColor: '#95c943',
                            top: 0,
                            right: 0,
                            left: 24,
                          }}>
                          <Text
                            style={{
                              fontSize: 8,
                              textAlign: 'center',
                              fontWeight: 'bold',
                              color: '#FFFF',
                              top: 1,
                            }}>
                            0
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />
          </View>
        )}

        {newOnlineStores && newOnlineStores.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 20,
                textAlign: 'center',
              }}>
              NEW ONLINE STORE
            </Text>
            <FlatList
              data={newOnlineStores}
              contentContainerStyle={{
                marginTop: 20,
                padding: 16,
                backgroundColor: '#FDE3E6',
              }}
              horizontal={true}
              keyExtractor={(item, index) => index}
              renderItem={({item}) => (
                <View>
                  {item.deal_images && (
                    <View
                      style={[
                        styles.imageContainer,
                        {
                          width: width - 100,
                          height: width / 2,
                          marginStart: 5,
                        },
                      ]}>
                      <Image
                        resizeMode={'contain'}
                        style={{width: width - 80, height: width / 2}}
                        source={{
                          uri: `${CommonConstants.IMAGE_URL}/images/product_image/${item.deal_images}`,
                        }}
                      />
                    </View>
                  )}
                </View>
              )}
            />
          </View>
        )}

        {weekelyEventsList && weekelyEventsList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 20,
                textAlign: 'center',
              }}>
              WEEKLY EVENTS
            </Text>
            <FlatList
              data={weekelyEventsList}
              contentContainerStyle={{
                marginTop: 20,
                backgroundColor: '#FDE3E6',
                marginHorizontal: 10,
                borderRadius: 10,
                padding: 10,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item, index}) => (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      flex: 1,
                      flexDirection: 'column',
                      margin: 1,
                      backgroundColor: '#FFFF',
                      borderRadius: 10,
                      marginHorizontal: 5,
                      marginTop: 10,
                      padding: 10,
                    },
                  ]}>
                  <Image
                    resizeMode={'contain'}
                    style={[
                      styles.imageThumbnail,
                      {
                        width: '100%',
                        height: 150,
                      },
                    ]}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/images/event_images/${item.event_image}`,
                    }}
                  />
                  <View style={{marginVertical: 20, alignItems: 'center'}}>
                    <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                      {`${item.event_title}`}
                    </Text>
                    <Text style={{color: '#b8b1a6', marginTop: 5}}>
                      {`Event location: ${item.event_location}`}
                    </Text>
                  </View>
                </View>
              )}
            />
          </View>
        )}
        {offersList && offersList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 30,
                textAlign: 'center',
              }}>
              OFFERS OF THE CITY
            </Text>
            <FlatList
              data={offersList}
              contentContainerStyle={{
                marginTop: 20,
                backgroundColor: '#FDE3E6',
                padding: 16,
                right: 5,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item}) => (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      width: '100%',
                      backgroundColor: '#FFFF',
                      borderRadius: 10,
                      marginHorizontal: 5,
                      padding: 10,
                      marginTop: 10,
                    },
                  ]}>
                  <Image
                    resizeMode={'contain'}
                    style={{
                      width: '100%',
                      height: 160,
                    }}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/images/offer_image/${item.offer_image}`,
                    }}
                  />
                  {item.company_name && (
                    <Text
                      style={{
                        fontSize: 16,
                        marginVertical: 10,
                        textAlign: 'center',
                      }}>
                      {item.company_name}
                    </Text>
                  )}
                </View>
              )}
            />
          </View>
        )}
        {brandAdsList && brandAdsList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 20,
                textAlign: 'center',
              }}>
              MOST BRAND COMPANY
            </Text>
            <FlatList
              data={brandAdsList}
              contentContainerStyle={{
                marginTop: 20,
                backgroundColor: '#FDE3E6',
                borderRadius: 10,
                padding: 10,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item, index}) => (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      flex: 1,
                      flexDirection: 'column',
                      margin: 1,
                      backgroundColor: '#FFFF',
                      borderRadius: 10,
                      marginTop: 10,
                      padding: 10,
                    },
                  ]}>
                  <Image
                    resizeMode={'contain'}
                    style={[
                      styles.imageThumbnail,
                      {
                        width: '100%',
                        height: 160,
                      },
                    ]}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/ads_banner/${item.ads_image_name}`,
                    }}
                  />
                </View>
              )}
            />
          </View>
        )}
        {articalsList && articalsList.length > 0 && (
          <View style={{flex: 1, marginBottom: 20}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 40,
                textAlign: 'center',
              }}>
              IMPROVE YOUR SKILLS
            </Text>
            <FlatList
              data={articalsList}
              contentContainerStyle={{
                marginTop: 20,
                backgroundColor: '#FDE3E6',
                padding: 10,
              }}
              horizontal={true}
              keyExtractor={(item, index) => index}
              renderItem={({item}) => (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      width: width - 160,
                      backgroundColor: '#FFFF',
                      height: width / 2,
                      borderRadius: 10,
                      marginHorizontal: 5,
                    },
                  ]}>
                  <Image
                    resizeMode={'contain'}
                    style={{width: width - 180, height: 160}}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/images/article_image/${item.article_image}`,
                    }}
                  />
                </View>
              )}
            />
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fdedee',
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default AfterLogin;
