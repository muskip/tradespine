/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {signOut} from '../redux/action/UserAction';

function Notification(props) {
  const dispatch = useDispatch();

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{alignItems: 'center', justifyContent: 'center', marginTop: 50}}>
        <Text>Notification page</Text>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Notification;
