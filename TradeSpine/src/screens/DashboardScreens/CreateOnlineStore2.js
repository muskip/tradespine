/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {pickImage} from '../../utils';
import ImagePicker from '../../components/ImagePicker';
import CheckBox from '@react-native-community/checkbox';
import ImageUpload from '../../components/ImageUpload';

function CreateOnlineStore2(props) {
  const [firstImage, setFirstImage] = React.useState(undefined);
  const [secondImage, setSecondImage] = React.useState(undefined);
  const [thirdImage, setThirdImage] = React.useState(undefined);
  const [fourthImage, setFourthImage] = React.useState(undefined);
  const [fifthImage, setFifthImage] = React.useState(undefined);
  const [sixthImage, setSixthImage] = React.useState(undefined);
  const [isAcceptConditions, setIsAcceptConditions] = React.useState(false);
  let imageFor = '';
  const imagePickerRef = React.useRef(null);

  const toggleImagePicker = () => {
    imagePickerRef.current?.open();
  };

  const chooseImage = type => {
    imageFor = type;
    toggleImagePicker();
  };

  const pick = type => {
    let pickerOptions = {
      cropping: true,
      cropperToolbarTitle: 'Scale and move with fingers to crop',
      hideBottomControls: true,
    };
    pickerOptions = {...pickerOptions, width: 640, height: 480};
    pickImage(type, pickerOptions)
      .then(handlePickedImage)
      .catch(error => {
        if (error && error.code === 'E_PERMISSION_MISSING') {
          Alert.alert('', 'Permission Missing');
        }
      });
  };

  const handlePickedImage = image => {
    let croppedImage;
    const parts = image.path.split('/');
    const fileName = parts[parts.length - 1];
    croppedImage = {
      uri: image.path,
      type: image.mime,
      name: fileName,
    };
    switch (imageFor) {
      case 'image1':
        setFirstImage(croppedImage);
        return;
      case 'image2':
        setSecondImage(croppedImage);
        return;
      case 'image3':
        setThirdImage(croppedImage);
        return;
      case 'image4':
        setFourthImage(croppedImage);
        return;
      case 'image5':
        setFifthImage(croppedImage);
        return;
      case 'image6':
        setSixthImage(croppedImage);
        return;
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{flex: 1}}>
        <View style={{flex: 1, marginHorizontal: 10}}>
          <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 30}}>
            Upload Store Images
          </Text>

          <View style={{flexDirection: 'row'}}>
            <ImageUpload
              style={{marginTop: 12}}
              onPress={() => chooseImage('image1')}
              selectedImage={firstImage}
            />
            <ImageUpload
              style={{marginTop: 12, marginLeft: 10}}
              onPress={() => chooseImage('image2')}
              selectedImage={secondImage}
            />
            <ImageUpload
              style={{marginTop: 12, marginLeft: 10}}
              onPress={() => chooseImage('image3')}
              selectedImage={thirdImage}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <ImageUpload
              style={{marginTop: 12}}
              onPress={() => chooseImage('image4')}
              selectedImage={fourthImage}
            />
            <ImageUpload
              photoLabel={'test'}
              style={{marginTop: 12, marginLeft: 10}}
              onPress={() => chooseImage('image5')}
              selectedImage={fifthImage}
            />
            <ImageUpload
              photoLabel={'test'}
              style={{marginTop: 12, marginLeft: 10}}
              onPress={() => chooseImage('image6')}
              selectedImage={sixthImage}
            />
          </View>
          <TouchableOpacity
            style={[styles.btn, {width: '100%', borderRadius: 8}]}>
            <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
              Create Online Store
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 20,
            }}>
            <CheckBox
              disabled={false}
              value={isAcceptConditions}
              onValueChange={newValue => setIsAcceptConditions(newValue)}
            />
            <Text style={{fontSize: 14, color: '#393939', marginTop: 5}}>
              I accept terms and conditions.
            </Text>
          </View>
        </View>
      </ScrollView>
      <ImagePicker
        ref={imagePickerRef}
        onCameraSelect={() => pick('camera')}
        onLibrarySelect={() => pick('library')}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 5,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderColor: '#d3cec4',
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 46,
    color: 'black',
  },
  ImageStyle: {
    height: 20,
    width: 20,
    margin: 12,
    tintColor: '#C0C0C0',
  },
  btn: {
    alignItems: 'center',
    padding: 10,
    marginTop: 40,
    width: '90%',
    height: 44,
    backgroundColor: '#BA2525',
    marginBottom: 20,
  },
});

export default CreateOnlineStore2;
