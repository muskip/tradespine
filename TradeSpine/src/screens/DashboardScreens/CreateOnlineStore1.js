/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {pickImage} from '../../utils';
import ImagePicker from '../../components/ImagePicker';
import ImageUpload from '../../components/ImageUpload';

function CreateOnlineStore1(props) {
  const [storeNam, setStoreName] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [gstNo, setGstNo] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [mobileNo, setMobileNo] = React.useState('');
  const [date, setDate] = React.useState('');
  const [address1, setAddress1] = React.useState('');
  const [address2, setAddress2] = React.useState('');
  const [pincode, setPincode] = React.useState('');

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{flex: 1}}>
        <View style={{flex: 1, marginHorizontal: 10}}>
          <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 40}}>
            Create Online Store
          </Text>
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setStoreName}
            value={setStoreName}
            placeholder={'Name of the store'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30, height: 100}]}
            onChangeText={setDescription}
            value={description}
            placeholder={'Description'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setGstNo}
            value={gstNo}
            placeholder={'GST no/PAN no'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setEmail}
            value={email}
            placeholder={'Email id'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={mobileNo}
            value={setMobileNo}
            placeholder={'Mobile Number'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setDate}
            value={date}
            placeholder={'Business Establishment date'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setAddress1}
            value={address1}
            placeholder={'Address line 1'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setAddress2}
            value={address2}
            placeholder={'Address line 2'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setPincode}
            value={pincode}
            placeholder={'Pincode'}
          />
          <TouchableOpacity
            style={[styles.btn, {width: '100%', borderRadius: 8}]}>
            <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
              Next
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 5,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderColor: '#d3cec4',
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 46,
    color: 'black',
  },
  ImageStyle: {
    height: 20,
    width: 20,
    margin: 12,
    tintColor: '#C0C0C0',
  },
  btn: {
    alignItems: 'center',
    padding: 10,
    marginTop: 40,
    width: '90%',
    height: 44,
    backgroundColor: '#BA2525',
    marginBottom: 20,
  },
});

export default CreateOnlineStore1;
