import React, {useState} from 'react';
import {StyleSheet, Image, View, Text} from 'react-native';

export default function NoInternetConnection() {
  const [refresh, setrefresh] = useState();

  return (
    <View
      accessible={false}
      // eslint-disable-next-line react-native/no-inline-styles
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        width: '100%',
        zIndex: 9,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View style={styles.container}>
        <Text style={{fontSize:18, marginTop:50}}>No internet connection</Text>
        <Image
          style={{height: 80, width: 80, marginTop:40}}
          source={require('../../assets/images/connection.png')}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent:'center'
  },
});
