/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Platform,
  Image,
  Dimensions,
} from 'react-native';
import {FlatListSlider} from 'react-native-flatlist-slider';
import {ImagePreview} from '../../components/ImagePreview';
import Loader from '../../components/Loader';
import APIRequest from '../../network/APIRequest';
import {CommonConstants} from '../../constants/index';

function MainDashboard(props) {
  const [topHeaderItemsList, setTopHeaderItemsList] = React.useState([]);
  const [sliderOneList, setSliderOneList] = React.useState([]);
  const [sliderTwoList, setSliderTwoList] = React.useState([]);
  const [banner4Data, setBanner4Data] = React.useState([]);
  const [level5Data, setLevel5Data] = React.useState([]);
  const [level6Data, setLevel6Data] = React.useState([]);
  const [level7Data, setLevel7Data] = React.useState([]);
  const [storeBenefitsData, setStoreBenefitsData] = React.useState([]);
  const [advertisementsData, setAdvertisementsData] = React.useState([]);
  const [isLoading, setLoading] = React.useState(false);
  const width = Dimensions.get('window').width;

  useEffect(() => {
    getTopHeaderData();
    getSliderOneData();
    getSlidertwoData();
    getBanner4Data();
    getLevel5Data();
    getLevel6Data();
    getLevel7Data();
    getStoreBenefitsData();
    getAdvertisementsData();
  }, []);

  function getTopHeaderData() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/home_page_top_header', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        // console.log('getTopHeaderData', res);
        setLoading(false);
        setTopHeaderItemsList(res?.result);
      });
  }
  function getSliderOneData() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/home_page_content_slider_one', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        console.log("res--->>>>>>>>>>>>>",res?.result);
        setLoading(false);
        setSliderOneList(res?.result);
      });
  }
  function getSlidertwoData() {
    setLoading(true);
    fetch('https://www.tradespine.co.in/json/home_page_content_slider_two', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        setSliderTwoList(res?.result);
      });
  }

  function getBanner4Data() {
    setLoading(true);
    fetch(
      'https://www.tradespine.co.in/json/home_page_content_featured_slider',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
        },
      },
    )
      .then(response => response.json())
      .then(res => {
        console.log("res --banner4", res.result);
        setLoading(false);
        setBanner4Data(res?.result);
      });
  }

  function getLevel5Data() {
    setLoading(true);
    APIRequest.get(
      'https://www.tradespine.co.in/json/home_page_content_image_1',
      response => {
        if (response) {
          setLoading(false);
          setLevel5Data(response?.result);
        }
      },
    );
  }

  function getLevel6Data() {
    setLoading(true);
    APIRequest.get(
      'https://www.tradespine.co.in/json/home_page_content_image_4',
      response => {
        if (response) {
          setLoading(false);
          setLevel6Data(response?.result);
        }
      },
    );
  }

  function getLevel7Data() {
    setLoading(true);
    APIRequest.get(
      'https://www.tradespine.co.in/json/home_page_content_image_6',
      response => {
        if (response) {
          setLoading(false);
          setLevel7Data(response?.result);
        }
      },
    );
  }

  function getStoreBenefitsData() {
    setLoading(true);
    fetch(
      'https://www.tradespine.co.in/json/home_page_content_slider_benefit_of_online_store',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
        },
      },
    )
      .then(response => response.json())
      .then(res => {
        console.log("res--->>>benefirs", res.result);
        setLoading(false);
        setStoreBenefitsData(res?.result);
      });
  }

  function getAdvertisementsData() {
    setLoading(true);
    fetch(
      'https://www.tradespine.co.in/json/home_page_content_slider_advertisement_in_india',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
        },
      },
    )
      .then(response => response.json())
      .then(res => {
        setLoading(false);
        setAdvertisementsData(res?.result);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <ScrollView>
        {topHeaderItemsList && topHeaderItemsList.length > 0 && (
          <View style={{flex: 1}}>
            <View style={{marginTop: 10}}>
              <FlatListSlider
                data={topHeaderItemsList}
                timer={5000}
                component={<ImagePreview containerWidth={width - 20} />}
                indicator={false}
                contentContainerStyle={{paddingHorizontal: 5}}
              />
            </View>
          </View>
        )}
        {sliderOneList && sliderOneList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 20,
                textAlign: 'center',
              }}>
              CREATE ONLINE STORE IN INDIA
            </Text>
            {sliderOneList.map(item => {
              return (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      marginTop: 30,
                      marginBottom: 20,
                      width: width - 10,
                      backgroundColor: '#FFFF',
                      height: width / 2,
                      borderRadius: 10,
                      marginHorizontal: 5,
                    },
                  ]}>
                  <Image
                    style={{width: width - 30, height: width / 2 - 20}}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/ads_banner/${item.ads_image_name}`,
                    }}
                  />
                </View>
              );
            })}
          </View>
        )}
        {sliderTwoList && sliderTwoList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 20,
                textAlign: 'center',
              }}>
              JUST ACCESS THE FEATURES OF TRADESPINE
            </Text>
            <View style={{marginTop: 10}}>
              <FlatListSlider
                data={sliderTwoList}
                timer={5000}
                component={<ImagePreview containerWidth={width - 120} />}
                indicator={false}
                contentContainerStyle={{paddingHorizontal: 5}}
              />
            </View>
          </View>
        )}

        {banner4Data && banner4Data.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 20,
                textAlign: 'center',
              }}>
              FEATURES OF TRADESPINE
            </Text>
            {banner4Data.map(item => {
              return (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      marginTop: 30,
                      marginBottom: 20,
                      width: width - 10,
                      backgroundColor: '#FFFF',
                      height: width / 2,
                      borderRadius: 10,
                      marginHorizontal: 5,
                    },
                  ]}>
                  <Image
                    style={{width: width - 30, height: width / 2 - 20}}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/ads_banner/${item.ads_image_name}`,
                    }}
                  />
                </View>
              );
            })}
          </View>
        )}
        {level5Data && level5Data.length > 0 && (
          <View>
            <FlatListSlider
              data={level5Data}
              timer={5000}
              component={<ImagePreview containerWidth={width - 230} />}
              indicator={false}
              contentContainerStyle={{paddingHorizontal: 5}}
            />
          </View>
        )}
        {level6Data && level6Data.length > 0 && (
          <View>
            <FlatListSlider
              data={level6Data}
              timer={5000}
              component={<ImagePreview containerWidth={width - 200} />}
              indicator={false}
              contentContainerStyle={{paddingHorizontal: 5}}
            />
          </View>
        )}
        {level7Data && level7Data.length > 0 && (
          <View>
            <FlatListSlider
              data={level7Data}
              timer={5000}
              component={<ImagePreview containerWidth={width - 160} />}
              indicator={false}
              contentContainerStyle={{paddingHorizontal: 5}}
            />
          </View>
        )}

        {storeBenefitsData && storeBenefitsData.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 40,
                textAlign: 'center',
              }}>
              BENEFITS OF ONLINE STORE IN INDIA
            </Text>
            {storeBenefitsData.map(item => {
              return (
                <View
                  style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      marginTop: 30,
                      marginBottom: 20,
                      width: width - 10,
                      backgroundColor: '#FFFF',
                      height: width / 2,
                      borderRadius: 10,
                      marginHorizontal: 5,
                    },
                  ]}>
                  <Image
                    style={{width: width - 30, height: width / 2 - 20}}
                    source={{
                      uri: `${CommonConstants.IMAGE_URL}/ads_banner/${item.ads_image_name}`,
                    }}
                  />
                </View>
              );
            })}
          </View>
        )}

        {advertisementsData && advertisementsData.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 40,
                textAlign: 'center',
              }}>
              ADVERTISEMENT IN INDIA
            </Text>
            <View style={{marginTop: 10}}>
              <FlatListSlider
                data={advertisementsData}
                timer={5000}
                component={<ImagePreview containerWidth={width - 150} />}
                indicator={false}
                contentContainerStyle={{paddingHorizontal: 5}}
              />
            </View>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFE6E6',
  },
  card: {
    marginTop: 20,
    borderRadius: 10,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 0,
    padding: 10,
    backgroundColor: '#ffff',
  },

  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
});

export default MainDashboard;
