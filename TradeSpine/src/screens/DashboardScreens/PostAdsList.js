import React, {useState} from 'react';

// import all the components we are going to use
import {SafeAreaView, StyleSheet, View, FlatList, Image} from 'react-native';

function PostAdsList() {
  const [dataSource, setDataSource] = useState([
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
    {image: 'https://picsum.photos/200'},
  ]);

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={dataSource}
        renderItem={({item}) => (
          <View
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              flex: 1,
              flexDirection: 'column',
              margin: 5,
            }}>
            <Image style={styles.imageThumbnail} source={{uri: item.image}} />
          </View>
        )}
        numColumns={2}
        keyExtractor={(item, index) => index}
      />
    </SafeAreaView>
  );
}
export default PostAdsList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    margin: 5,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
  },
});
