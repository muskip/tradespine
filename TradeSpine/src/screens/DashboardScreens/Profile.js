import React, {useState} from 'react';
import {StyleSheet, Image, View, Text} from 'react-native';
import {CommonConstants} from '../../constants/index';

export default function Profile() {
  const [refresh, setrefresh] = useState();

  return (
      <View style={styles.container}>
                <View style={[
                    styles.imageContainer,
                    styles.shadow,
                    {
                      flexDirection: 'column',
                      backgroundColor: '#FFFF',
                      borderRadius: 10,
                      marginTop: 40,
                      padding: 10,
                      width: '90%',
                    },
                  ]}>
                  <Image
                    resizeMode={'contain'}
                    style={[
                      styles.imageThumbnail,
                      {
                        width: '100%',
                        height: 150,
                      },

                    ]}
                    source={{
                      uri: `https://i.picsum.photos/id/362/200/300.jpg?hmac=YjZiJWaqrdKL4xFhgrjDw4Ic2tPzNLV975FWRb8td0s`,
                    }}
                  />
                  <View style={{marginBottom:20}}>
                    <Text style={{fontSize: 26, fontWeight: 'bold', marginTop:40, textAlign:'center'}}>
                      {`VR India trader`}
                    </Text>
                    <Text style={{fontSize: 18, marginTop:10, textAlign:'center'}}>
                      {`storename@gmail.com`}
                    </Text>
                    <Text style={{fontSize: 18, marginTop:10, textAlign:'center'}}>
                      {`+91 877777777`}
                    </Text>
                    <Text style={{fontSize: 18, marginTop:10, textAlign:'center'}}>
                      {`Rajasthan, jaipur`}
                    </Text>
                    <Text style={{color: '#b8b1a6', marginTop: 5, textAlign:'center'}}>
                      {`Description hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh`}
                    </Text>
                  </View>
                </View>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
  },
  imageContainer: {
    marginTop:40
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
});
