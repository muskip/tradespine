/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  FlatList,
  ScrollView,
  Dimensions,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import APIRequest from '../../network/APIRequest';
import {CommonConstants} from '../../constants/index';
import Loader from '../../components/Loader';

export default function ContactsList(props) {
  const userId = useSelector(state => state.auth.userId);
  const [contacts, setContactsList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(false);
  const width = Dimensions.get('window').width;

  useEffect(() => {
    getContactsList();
  }, []);

  function getContactsList() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/contact_list',
      formdata,
      response => {
        if (response) {
          console.log('response contacts', response);
          setLoading(false);
          setContactsList(response?.result);
        }
      },
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <ScrollView>
        {contacts && contacts.length > 0 && (
          <View style={{flex: 1}}>
            <FlatList
              data={contacts}
              contentContainerStyle={{
                marginBottom: 20,
                marginHorizontal: 10,
                padding: 10,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item, index}) => {
                return (
                  <View
                    style={[
                      styles.shadow,
                      {
                        flex: 1,
                        margin: 1,
                        backgroundColor: '#FFFF',
                        marginHorizontal: 5,
                        marginTop: 10,
                        padding: 10,
                      },
                    ]}>
                    <Image
                      resizeMode={'contain'}
                      style={[
                        styles.imageThumbnail,
                        {
                          width: '100%',
                          height: 150,
                          backgroundColor: '#f6f2ef',
                        },
                      ]}
                      source={{
                        uri: item.contact_images
                          ? `${CommonConstants.IMAGE_URL}/images/store_images/${item.store_image_name}`
                          : `${CommonConstants.IMAGE_URL}/images/sellers.jpg`,
                      }}
                    />
                    <View style={{marginHorizontal: 10, marginTop: 20}}>
                      <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                        {`${item?.full_name ?? ''}`}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginStart: 2,
                          color: '#b8b1a6',
                          marginTop: 10,
                        }}>
                        {`${item?.mobileno ?? ''}`}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginStart: 2,
                          color: '#b8b1a6',
                          marginTop: 10,
                        }}>
                        {`${item?.address_line1 ?? ''} ${
                          item?.mobileno ?? ''
                        }`}
                      </Text>
                    </View>
                  </View>
                );
              }}
            />
          </View>
        )}
        <View
          style={{
            marginBottom: 20,
            paddingHorizontal: 16,
            paddingBottom: 10,
            marginHorizontal:12
          }}>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 80,
    color: 'black',
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
