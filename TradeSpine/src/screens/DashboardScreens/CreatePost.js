/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {pickImage} from '../../utils';
import ImagePicker from '../../components/ImagePicker';
import ImageUpload from '../../components/ImageUpload';

function CreatePost(props) {
  const [title, setTitle] = React.useState('');
  const [subTitle, setSubTitle] = React.useState('');
  const [postContent, setPostContent] = React.useState('');
  const [firstImage, setFirstImage] = React.useState(undefined);
  let imageFor = '';
  const imagePickerRef = React.useRef(null);

  const toggleImagePicker = () => {
    imagePickerRef.current?.open();
  };

  const chooseImage = type => {
    imageFor = type;
    toggleImagePicker();
  };

  const pick = type => {
    let pickerOptions = {
      cropping: true,
      cropperToolbarTitle: 'Scale and move with fingers to crop',
      hideBottomControls: true,
    };
    pickerOptions = {...pickerOptions, width: 640, height: 480};
    pickImage(type, pickerOptions)
      .then(handlePickedImage)
      .catch(error => {
        if (error && error.code === 'E_PERMISSION_MISSING') {
          Alert.alert('', 'Permission Missing');
        }
      });
  };

  const handlePickedImage = image => {
    let croppedImage;
    const parts = image.path.split('/');
    const fileName = parts[parts.length - 1];
    croppedImage = {
      uri: image.path,
      type: image.mime,
      name: fileName,
    };
    switch (imageFor) {
      case 'image1':
        setFirstImage(croppedImage);
        return;
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{flex: 1}}>
        <View style={{flex: 1, marginHorizontal: 10}}>
          <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 40}}>
            Create a Post
          </Text>
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setTitle}
            value={title}
            placeholder={'Title'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30}]}
            onChangeText={setSubTitle}
            value={subTitle}
            placeholder={'Sub Title'}
          />
          <TextInput
            style={[styles.input, {marginTop: 30, height: 100}]}
            onChangeText={setPostContent}
            value={postContent}
            placeholder={'Add Post Content'}
          />
          <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 30}}>
            Upload Featured Image
          </Text>

          <View style={{flexDirection: 'row'}}>
            <ImageUpload
              style={{marginTop: 12}}
              onPress={() => chooseImage('image1')}
              selectedImage={firstImage}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              style={[
                styles.btn,
                {
                  width: '45%',
                  backgroundColor: '#FFFF',
                  borderColor: '#BA2525',
                  borderWidth: 1,
                },
              ]}>
              <Text
                style={{fontSize: 18, color: '#BA2525', fontWeight: 'bold'}}>
                Preview
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.btn, {width: '45%'}]}>
              <Text style={{fontSize: 18, color: '#FFFF', fontWeight: 'bold'}}>
                Post
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
      <ImagePicker
        ref={imagePickerRef}
        onCameraSelect={() => pick('camera')}
        onLibrarySelect={() => pick('library')}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 5,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderColor: '#d3cec4',
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 46,
    color: 'black',
  },
  ImageStyle: {
    height: 20,
    width: 20,
    margin: 12,
    tintColor: '#C0C0C0',
  },
  btn: {
    alignItems: 'center',
    padding: 10,
    marginTop: 40,
    width: '90%',
    height: 44,
    backgroundColor: '#BA2525',
    marginBottom: 20,
  },
});

export default CreatePost;
