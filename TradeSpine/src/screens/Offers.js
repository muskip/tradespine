/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  FlatList,
  ScrollView,
  Dimensions,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import APIRequest from '../network/APIRequest';
import {CommonConstants} from '../constants/index';
import Loader from '../components/Loader';
import {FlatListSlider} from 'react-native-flatlist-slider';
import {ImagePreview} from '../components/ImagePreview';
import AdsImage from '../components/AdsImage';
import {getAdsIndex} from '../utils/index';

function Offers(props) {
  const userId = useSelector(state => state.auth.userId);
  const [offersList, setOffersList] = React.useState([]);
  const [offersAdsList, setOffersAdsList] = React.useState([]);
  const [topHeaderItemsList, setTopHeaderItemsList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(false);
  const width = Dimensions.get('window').width;
  const [adsCurrentIndex, setAdsCurrentIndex] = React.useState(null);
  let tempList = [];
  let sum = 0;
  const isShowMoreAds = adsCurrentIndex
    ? offersAdsList.length > adsCurrentIndex + 1
    : offersAdsList.length
    ? true
    : false;
  // console.log('isShowMoreAds', isShowMoreAds);
  // console.log('offersAdsList', offersAdsList);

  useEffect(() => {
    getTopHeaderData();
    getOffers();
    getOffersAds();
  }, []);

  function getTopHeaderData() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/top_ads_todaydeals',
      formdata,
      response => {
        if (response) {
          console.log('response top_ads_wholeseller', response);
          setLoading(false);
          setTopHeaderItemsList(response?.result);
        }
      },
    );
  }

  function getOffersAds() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/all_online_store_ads',
      formdata,
      response => {
        if (response) {
          console.log('offers adsss--->>>>>', response);
          setLoading(false);
          setOffersAdsList(response?.result);
        }
      },
    );
  }

  function getOffers() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/today_deals',
      formdata,
      response => {
        if (response) {
          setLoading(false);
          console.log('response offers', response);
          setOffersList(response?.result);
        }
      },
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <ScrollView>
        {topHeaderItemsList && topHeaderItemsList.length > 0 && (
          <View style={{flex: 1}}>
            <View>
              <FlatListSlider
                data={topHeaderItemsList}
                timer={5000}
                component={<ImagePreview containerWidth={width - 10} />}
                indicator={false}
                contentContainerStyle={{paddingHorizontal: 5}}
              />
            </View>
          </View>
        )}
        {offersList && offersList.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 22,
                fontWeight: 'bold',
                marginTop: 10,
                marginBottom: 20,
                textAlign: 'center',
              }}>
              TODAY'S DEAL
            </Text>
            <FlatList
              data={offersList}
              contentContainerStyle={{
                marginBottom: 20,
                padding: 10,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item, index}) => {
                let adsIndex = null;
                const i = index > 0 ? 3 : 2;
                sum = sum + i;
                tempList.push(sum);
                const isAdsIndex = tempList.includes(index);
                if (isAdsIndex) {
                  adsIndex = getAdsIndex(index);
                  if (offersAdsList[adsIndex]) {
                    setAdsCurrentIndex(adsIndex);
                  }
                }
                return isAdsIndex && offersAdsList[adsIndex] ? (
                  <AdsImage item={offersAdsList[adsIndex]} />
                ) : (
                  <View
                    style={[
                      styles.shadow,
                      {
                        flex: 1,
                        flexDirection: 'column',
                        backgroundColor: '#FFFF',
                        marginHorizontal: 5,
                        marginVertical: 10,
                        padding: 10,
                      },
                    ]}>
                    <Image
                      resizeMode={'contain'}
                      style={[
                        {
                          width: '100%',
                          height: 150,
                          backgroundColor: '#f6f2ef',
                        },
                      ]}
                      source={{
                        uri: item.thumbnail
                          ? `${CommonConstants.IMAGE_URL}/images/store_images/thumbnail/${item.thumbnail}`
                          : `${CommonConstants.IMAGE_URL}/images/sellers.jpg`,
                      }}
                    />
                    <View style={{marginHorizontal: 10, marginTop: 20}}>
                      <View
                        style={{
                          backgroundColor: '#ba2525',
                          paddingHorizontal: 5,
                          paddingVertical: 5,
                          width: '60%',
                        }}>
                        <Text
                          style={{
                            fontSize: 16,
                            color: '#FFFF',
                            textAlign: 'center',
                          }}>
                          {'DEAL OF THE DAY'}
                        </Text>
                      </View>
                      <View style={{marginTop: 20}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                          {`${item?.product_name?.toUpperCase() ?? ''}`}
                        </Text>
                        <View style={{marginTop: 10, flexDirection: 'row'}}>
                          <View style={{flexDirection: 'row'}}>
                            <Image
                              source={require('../assets/images/rupee.png')}
                              style={{
                                width: 20,
                                height: 20,
                                alignSelf: 'center',
                                tintColor: '#000',
                              }}
                            />
                            <Text
                              style={{
                                fontSize: 22,
                                marginStart: 3,
                                color: '#000',
                                marginBottom: 2,
                                fontWeight: 'bold',
                              }}>
                              {`${item?.offer_price ?? ''}`}
                            </Text>
                          </View>
                          <View style={{flexDirection: 'row', marginStart: 10}}>
                            <Image
                              source={require('../assets/images/rupee.png')}
                              style={{
                                width: 12,
                                height: 12,
                                alignSelf: 'center',
                                tintColor: '#a9a499',
                              }}
                            />
                            <Text
                              style={{
                                fontSize: 14,
                                color: '#a9a499',
                                fontWeight: 'bold',
                                marginTop: 5,
                              }}>
                              {`${item?.offer_price ?? ''}`}
                            </Text>
                          </View>
                        </View>
                        <Text
                          style={{
                            fontSize: 14,
                            marginTop: 5,
                            color: '#ba2525',
                          }}>
                          {'OFFER VALID FOR 2 DAYS'}
                        </Text>
                        <Text
                          style={{
                            fontSize: 18,
                            marginTop: 16,
                            color: '#2c4006',
                          }}>
                          {'Description'}
                        </Text>
                        <Text
                          style={{
                            fontSize: 16,
                            marginVertical: 10,
                            color: '#000',
                          }}>
                          {`${item?.description ?? ''}`}
                        </Text>
                        <View style={{flexDirection: 'row', marginTop: 10}}>
                          <Image
                            source={require('../assets/images/rightArrow.png')}
                            style={{
                              width: 12,
                              height: 12,
                              alignSelf: 'center',
                              tintColor: '#b92121',
                            }}
                          />
                          <Text
                            style={{
                              fontSize: 16,
                              marginStart: 5,
                              color: '#b92121',
                              marginBottom: 2,
                            }}>
                            {'See more product details'}
                          </Text>
                        </View>
                        <View style={{marginVertical: 10}}>
                          <Text
                            style={{
                              fontSize: 16,
                              color: '#514940',
                            }}>
                            {`Company Name: ${item?.company_name ?? ''}`}
                          </Text>
                          <Text
                            style={{
                              fontSize: 16,
                              color: '#514940',
                              marginVertical: 10,
                            }}>
                            {`Company Address: ${item?.business_adress ?? ''}`}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                );
              }}
            />
          </View>
        )}
        <View
          style={{
            marginBottom: 20,
            paddingHorizontal: 16,
            paddingBottom: 10,
            marginHorizontal:12
          }}>
          {isShowMoreAds &&
            offersAdsList &&
            offersAdsList.length &&
            offersAdsList.map((item, index) => {
              console.log('item', item);
              if (adsCurrentIndex) {
                if (index > adsCurrentIndex) {
                  return <AdsImage item={offersAdsList[index]} />;
                }
              } else {
                return <AdsImage item={offersAdsList[index]} />;
              }
            })}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 80,
    color: 'black',
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Offers;
