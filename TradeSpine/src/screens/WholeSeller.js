/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  FlatList,
  ScrollView,
  Dimensions,
  Platform,
  TextInput,
} from 'react-native';
import {useSelector} from 'react-redux';
import APIRequest from '../network/APIRequest';
import {CommonConstants} from '../constants/index';
import Loader from '../components/Loader';
import {FlatListSlider} from 'react-native-flatlist-slider';
import {ImagePreview} from '../components/ImagePreview';
import AdsImage from '../components/AdsImage';
import {getAdsIndex} from '../utils/index';

function WholeSeller(props) {
  const userId = useSelector(state => state.auth.userId);
  const [topHeaderItemsList, setTopHeaderItemsList] = React.useState([]);
  const [wholeSellersAdList, setWholeSellerAdsList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(false);
  const [wholeSellers, setWholeSellers] = React.useState([]);
  const width = Dimensions.get('window').width;
  const [adsCurrentIndex, setAdsCurrentIndex] = React.useState(null);
  let tempList = [];
  let sum = 0;
  console.log('wholeSellersAdList', wholeSellersAdList);
  console.log('adsCurrentIndex', adsCurrentIndex);
  const isShowMoreAds = adsCurrentIndex
    ? wholeSellersAdList.length > adsCurrentIndex + 1
    : wholeSellersAdList.length
    ? true
    : false;
  // console.log('isShowMoreAds', isShowMoreAds);

  useEffect(() => {
    getTopHeaderData();
    getWholeSellerData();
    getWholeSellerAds();
  }, []);

  function getTopHeaderData() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/top_ads_wholeseller_page',
      formdata,
      response => {
        if (response) {
          console.log('response top_ads_wholeseller', response);
          setLoading(false);
          setTopHeaderItemsList(response?.result);
        }
      },
    );
  }

  function getWholeSellerAds() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/wholeseller_ads',
      formdata,
      response => {
        if (response) {
          console.log(' wholeseller_ads adsss--->>>>>', response);
          setLoading(false);
          setWholeSellerAdsList(response?.result);
        }
      },
    );
  }

  function getWholeSellerData() {
    setLoading(true);
    var formdata = new FormData();
    formdata.append('userid', userId);
    APIRequest.postFormData(
      'https://www.tradespine.co.in/json/wholeseller',
      formdata,
      response => {
        if (response) {
          console.log('response WholeSeller', response);
          setLoading(false);
          setWholeSellers(response?.result);
        }
      },
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Loader isLoading={isLoading} />
      <ScrollView>
        {topHeaderItemsList && topHeaderItemsList.length > 0 && (
          <View style={{flex: 1}}>
            <View>
              <FlatListSlider
                data={topHeaderItemsList}
                timer={5000}
                component={<ImagePreview containerWidth={width - 10} />}
                indicator={false}
                contentContainerStyle={{paddingHorizontal: 5}}
              />
            </View>
          </View>
        )}
        {wholeSellers && wholeSellers.length > 0 && (
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 22,
                fontWeight: 'bold',
                marginTop: 10,
                marginBottom: 30,
                textAlign: 'center',
              }}>
              OUR BEST WHOLESELLERS
            </Text>
            <FlatList
              data={wholeSellers}
              contentContainerStyle={{
                marginBottom: 10,
                backgroundColor: '#FDE3E6',
                marginHorizontal: 10,
                borderRadius: 10,
                padding: 10,
              }}
              keyExtractor={(item, index) => index}
              renderItem={({item, index}) => {
                console.log('length', wholeSellers.length);
                let adsIndex = null;
                const i = index > 0 ? 3 : 2;
                sum = sum + i;
                tempList.push(sum);
                const isAdsIndex = tempList.includes(index);
                if (isAdsIndex) {
                  adsIndex = getAdsIndex(index);
                  if (wholeSellersAdList[adsIndex]) {
                    setAdsCurrentIndex(adsIndex);
                  }
                }
                return isAdsIndex && wholeSellersAdList[adsIndex] ? (
                  <AdsImage item={wholeSellersAdList[adsIndex]} />
                ) : (
                  <View
                    style={[
                      styles.shadow,
                      {
                        flex: 1,
                        flexDirection: 'column',
                        borderRadius: 10,
                        marginHorizontal: 5,
                        marginVertical: 10,
                        backgroundColor: '#FFFF',
                        padding: 10,
                      },
                    ]}>
                    <Image
                      resizeMode={'contain'}
                      style={[
                        styles.imageThumbnail,
                        {
                          width: '100%',
                          height: 150,
                          backgroundColor: '#f6f2ef',
                        },
                      ]}
                      source={{
                        uri: item.thumbnail
                          ? `${CommonConstants.IMAGE_URL}/images/store_images/thumbnail/${item.thumbnail}`
                          : `${CommonConstants.IMAGE_URL}/images/sellers.jpg`,
                      }}
                    />
                    <View style={{marginHorizontal: 10, marginTop: 20}}>
                      <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                        {`${item?.store_name?.toUpperCase() ?? ''}`}
                      </Text>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Image
                          source={require('../assets/images/location.png')}
                          style={{
                            width: 16,
                            height: 16,
                            alignSelf: 'center',
                            tintColor: '#a9a499',
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 14,
                            marginStart: 2,
                            color: '#b8b1a6',
                          }}>
                          {`${item?.address_line2 ?? ''}`}
                        </Text>
                      </View>
                      <Text
                        style={{
                          fontSize: 18,
                          marginTop: 10,
                        }}>
                        {'Member since 11 days'}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginTop: 20,
                          color: 'black',
                          fontWeight: 'bold',
                        }}>
                        {'Description'}
                      </Text>
                      <Text
                        style={{
                          fontSize: 14,
                          marginVertical: 10,
                          color: '#b8b1a6',
                        }}>
                        {`${item?.description ?? ''}`}
                      </Text>
                      {/* <TextInput
                      style={[styles.input, {marginVertical: 20}]}
                      onChangeText={setMessage}
                      value={message}
                      multiline={true}
                      numberOfLines={3}
                      placeholder={'Type Your Message here'}
                    /> */}
                    </View>
                  </View>
                );
              }}
            />
          </View>
        )}
        <View
          style={{
            backgroundColor: '#FDE3E6',
            marginBottom: 20,
            paddingHorizontal: 16,
            paddingBottom: 10,
            marginHorizontal:12
          }}>
          {isShowMoreAds &&
            wholeSellersAdList &&
            wholeSellersAdList.length &&
            wholeSellersAdList.map((item, index) => {
              console.log('item', item);
              if (adsCurrentIndex) {
                if (index > adsCurrentIndex) {
                  return <AdsImage item={wholeSellersAdList[index]} />;
                }
              } else {
                return <AdsImage item={wholeSellersAdList[index]} />;
              }
            })}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    borderRadius: 5,
    fontSize: 16,
    padding: 12,
    width: '100%',
    height: 80,
    color: 'black',
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default WholeSeller;
