/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import 'react-native-gesture-handler';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView,
  Alert,
} from 'react-native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import WholeSeller from '../screens/WholeSeller';
import Retailer from '../screens/Retailer';
import Offers from '../screens/Offers';
import CityTimeline from '../screens/CityTimeline';
import AfterLogin from '../screens/AfterLogin';
import Timeline from '../screens/Timeline';
import OnlineStore from '../screens/OnlineStore';
import PostAds from '../screens/PostAds';
import Webview from '../screens/Webview';
import {TabLabel} from './TabLabel';
import {signOut} from '../redux/action/UserAction';
import {useDispatch} from 'react-redux';
import CustomSideBarMenu from "../components/CustomSideBarMenu";

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HeaderLeft = props => {
  const toggleDrawer = () => {
    props.navigation.toggleDrawer();
  };

  return (
    <View style={{flexDirection: 'row'}}>
      <TouchableOpacity onPress={() => toggleDrawer()}>
        <Image
          source={require('../assets/images/menu.png')}
          style={{
            width: 20,
            height: 20,
            marginLeft: 10,
            tintColor: '#fff',
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

const HeaderRight = props => {
  return (
    <View style={{flexDirection: 'row', marginEnd: 20}}>
      <TouchableOpacity style={{alignSelf: 'center'}}>
        <Image
          source={require('../assets/images/location.png')}
          style={{
            width: 20,
            height: 20,
            marginEnd: 20,
            tintColor: '#fff',
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity style={{alignSelf: 'center'}}>
        <Image
          source={require('../assets/images/search.png')}
          style={{
            width: 20,
            height: 20,
            marginEnd: 20,
            tintColor: '#fff',
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          padding: 8,
          borderRadius: 8,
          backgroundColor: '#BE5963',
        }}
        onPress={() => props.navigation.navigate('PostAds')}>
        <Text style={{color: '#fff'}}>Post Ads</Text>
      </TouchableOpacity>
    </View>
  );
};

function firstScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="BottomTabStack">
      <Stack.Screen
        name="BottomTabStack"
        component={BottomTabStack}
        options={{
          title: '', //Set Header Title
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="PostAds"
        component={PostAds}
        options={{
          title: '', //Set Header Title
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
      <Stack.Screen name="Webview" component={Webview} />
    </Stack.Navigator>
  );
}

function secondScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="BottomTabStack">
      <Stack.Screen
        name="BottomTabStack"
        component={BottomTabStack}
        options={{
          title: '', //Set Header Title
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
      <Stack.Screen name="PostAds" component={PostAds} />
      <Stack.Screen name="Webview" component={Webview} />
    </Stack.Navigator>
  );
}

const defaultNavOptions = route => {
  return {
    tabBarLabel: ({focused}) => {
      let label;
      if (route.name === 'AfterLogin') {
        label = 'Home';
      } else if (route.name === 'WholeSeller') {
        label = 'Whole Seller';
      } else if (route.name === 'Retailer') {
        label = 'Retailer';
      } else if (route.name === 'Offers') {
        label = 'Offers';
      } else if (route.name === 'CityTimeline') {
        label = 'City Timeline';
      } else if (route.name === 'Timeline') {
        label = 'Timeline';
      } else if (route.name === 'OnlineStore') {
        label = 'Online Store';
      }
      return <TabLabel focused={focused} label={label} />;
    },
  };
};

const BottomTabStack = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => defaultNavOptions(route)}
      tabBarOptions={{
        activeBackgroundColor: '#F4717E',
        style: {
          backgroundColor: '#A75363',
        },
        tabStyle: {borderColor: '#F4717E', borderWidth: 2},
      }}>
      <Tab.Screen name="AfterLogin" component={AfterLogin} />
      <Tab.Screen name="WholeSeller" component={WholeSeller} />
      <Tab.Screen name="Retailer" component={Retailer} />
      <Tab.Screen name="Offers" component={Offers} />
      <Tab.Screen name="CityTimeline" component={CityTimeline} />
      <Tab.Screen name="Timeline" component={Timeline} />
      <Tab.Screen name="OnlineStore" component={OnlineStore} />
    </Tab.Navigator>
  );
};

function CustomDrawerContent(props) {
  const dispatch = useDispatch();
  return (
    <SafeAreaView style={{flex: 1}}>
      <TouchableOpacity style={{flexDirection:'row', marginVertical:20, marginStart:20}} onPress={()=> props.navigation.navigate('Dashboard')}>
      <Image
        source={require('../assets/images/user_profile.png')}
        style={{
          height: 12,
          width: 12,
          resizeMode: 'center',
          alignSelf: 'center',
        }}
      />
      <Text style={{marginStart:5}}>Jayprakash Singh</Text>
      </TouchableOpacity>
        
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} />
        <DrawerItem label={'Logout'} onPress={() => dispatch(signOut())} />
      </DrawerContentScrollView>
    </SafeAreaView>
  );
}

export default function HomeNavigator() {
  return (
    <Drawer.Navigator
      drawerStyle={{paddingHorizontal: 1, width:'60%'}}
      drawerContent={props => <CustomDrawerContent {...props} />}
      drawerContentOptions={{
        activeTintColor: '#C74F4F',
        itemStyle: {marginVertical: 5},
      }}>
      <Drawer.Screen
        name="FirstPage"
        options={{drawerLabel: 'First Page'}}
        component={firstScreenStack}
      />
      <Drawer.Screen
        name="SecondPage"
        options={{drawerLabel: 'Second Page'}}
        component={secondScreenStack}
      />
    </Drawer.Navigator>
  );
}
