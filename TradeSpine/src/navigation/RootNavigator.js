import * as React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SideDrawer from './SideDrawer';
import HomeNavigator from './HomeNavigator';
import DashboardDrawerNavigator from './DashboardDrawerNavigator';
import {useDispatch, useSelector} from 'react-redux';
import {restoreUser} from '../redux/action/UserAction';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonConstants} from '../constants';

export default function RootNavigator() {
  const Stack = createStackNavigator();
  const dispatch = useDispatch();
  const userToken = useSelector(state => state.auth.userToken);
  const [isLoading, setLoading] = React.useState(true);

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let token, user, userId;
      try {
        token = await AsyncStorage.getItem(CommonConstants.USER_TOKEN);
        user = await AsyncStorage.getItem(CommonConstants.USER_DATA);
        userId = await AsyncStorage.getItem(CommonConstants.USER_ID);
      } catch (e) {}
      setTimeout(() => {
        dispatch(
          restoreUser(JSON.parse(token), JSON.parse(user), JSON.parse(userId)),
        );
        setLoading(false);
      }, 1000);
    };
    bootstrapAsync();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const AuthStack = () => {
    return (
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="SideDrawer" component={SideDrawer} />
      </Stack.Navigator>
    );
  };

  const AppStack = () => {
    return (
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="Home" component={HomeNavigator} />
        <Stack.Screen name="Dashboard" component={DashboardDrawerNavigator} />
      </Stack.Navigator>
    );
  };

  return (
    <NavigationContainer>
      {!isLoading && (userToken == null ? <AuthStack /> : <AppStack />)}
    </NavigationContainer>
  );
}
