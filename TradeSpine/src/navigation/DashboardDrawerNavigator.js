/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import 'react-native-gesture-handler';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import MainDashboard from '../screens/DashboardScreens/MainDashboard';
import {createStackNavigator} from '@react-navigation/stack';
import AddContact from '../screens/DashboardScreens/AddContact';
import ContactsList from '../screens/DashboardScreens/ContactsList';
import CreateOnlineStore1 from '../screens/DashboardScreens/CreateOnlineStore1';
import FollowersList from '../screens/DashboardScreens/FollowersList';
import Profile from '../screens/DashboardScreens/Profile';
import Notification from '../screens/DashboardScreens/Notification';
import EditProduct from '../screens/DashboardScreens/EditProduct';
import PostAdsList from '../screens/DashboardScreens/PostAdsList';
import EditEvents1 from '../screens/DashboardScreens/EditEvents1';
import CreateTimeline from '../screens/DashboardScreens/CreateTimeline';
import CreateCityTimeline from '../screens/DashboardScreens/CreateCityTimeline';
import CreateTodaysDeal from '../screens/DashboardScreens/CreateTodaysDeal';
import CreateOnlineStore2 from '../screens/DashboardScreens/CreateOnlineStore2';


const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const HeaderLeft = props => {
  const toggleDrawer = () => {
    props.navigation.toggleDrawer();
  };

  return (
    <View style={{flexDirection: 'row'}}>
      <TouchableOpacity onPress={() => toggleDrawer()}>
        <Image
          source={require('../assets/images/menu.png')}
          style={{
            width: 20,
            height: 20,
            marginLeft: 10,
            tintColor: '#fff',
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

const HeaderRight = props => {
  return (
    <View style={{flexDirection: 'row', marginEnd: 20}}>
      <TouchableOpacity>
        <Image
          source={require('../assets/images/cart.png')}
          style={{
            width: 20,
            height: 20,
            tintColor: '#fff',
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

function ProfileScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="Profile">
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}
function ContactScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="Contacts">
      <Stack.Screen
        name="Contacts"
        component={ContactsList}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}
function followScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="Follow">
      <Stack.Screen
        name="Follow"
        component={FollowersList}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}

function NotificationScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="Notification">
      <Stack.Screen
        name="Notification"
        component={Notification}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}

function EditProductsListScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="EditProductsList">
      <Stack.Screen
        name="EditProductsList"
        component={EditProduct}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}

function PostAdsListScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="PostAdsList">
      <Stack.Screen
        name="PostAdsList"
        component={PostAdsList}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}
function EventsListScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="EventsList">
      <Stack.Screen
        name="EventsList"
        component={EditEvents1}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}

function OnlineStoreScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="OnlineStore">
      <Stack.Screen
        name="OnlineStore"
        component={CreateOnlineStore1}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}

function CityTimelineScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="CityTimeline">
      <Stack.Screen
        name="CityTimeline"
        component={CreateCityTimeline}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}

function TimelineScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="Timeline">
      <Stack.Screen
        name="Timeline"
        component={CreateTimeline}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}

function TodaysDealsScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="TodaysDeals">
      <Stack.Screen
        name="TodaysDeals"
        component={CreateTodaysDeal}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}

function PromoteOnlineStoreScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="PromoteOnlineStore">
      <Stack.Screen
        name="PromoteOnlineStore"
        component={CreateOnlineStore2}
        options={{
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
}




export default function DashboardDrawerNavigator() {
  return (
    <Drawer.Navigator
      drawerStyle={{marginTop: 55, paddingHorizontal: 5}}
      drawerContentOptions={{
        activeTintColor: '#C74F4F',
        itemStyle: {marginVertical: 5},
      }}>
      <Drawer.Screen
        name="FollowersList"
        options={{drawerLabel: 'FOLLOWERS LIST', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/follow.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={followScreenStack}
      />
      <Drawer.Screen
        name="ContactsList"
        options={{drawerLabel: 'CONTACTS LIST', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/user_profile.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={ContactScreenStack}
      />
      <Drawer.Screen
        name="Profile"
        options={{drawerLabel: 'PROFILE', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/user_profile.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={ProfileScreenStack}
      />
      <Drawer.Screen
        name="BucketNotification"
        options={{drawerLabel: 'BUCKET NOTIFICATION', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/notification.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={NotificationScreenStack}
      />
      <Drawer.Screen
        name="EditProductsList"
        options={{drawerLabel: 'EDIT PRODUCTS LIST', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/edit_product.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={EditProductsListScreenStack}
      />
      <Drawer.Screen
        name="PostAdsList"
        options={{drawerLabel: 'POST ADS LIST', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/edit_product.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={PostAdsListScreenStack}
      />
      <Drawer.Screen
        name="EventList"
        options={{drawerLabel: 'EVENT LIST', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/edit_product.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={EventsListScreenStack}
      />
      
      <Drawer.Screen
        name="OnlineStore"
        options={{drawerLabel: 'ONLINE STORE', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/store.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={OnlineStoreScreenStack}
      />
       <Drawer.Screen
        name="CreateCityTimeline"
        options={{drawerLabel: 'CREATE CITY TIMELINE', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/store.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={CityTimelineScreenStack}
      />

<Drawer.Screen
        name="CreateTimeline"
        options={{drawerLabel: 'CREATE TIMELINE', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/store.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={TimelineScreenStack}
      />

<Drawer.Screen
        name="CreateTodayDeals"
        options={{drawerLabel: 'CREATE TODAY DEALS', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/admin_chat.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={TodaysDealsScreenStack}
      />

<Drawer.Screen
        name="PromoteOnlineStore"
        options={{drawerLabel: 'PROMOTE ONLINE', drawerIcon: ({ focused, size }) => (
          <Image
          source={require('../assets/images/store.png')}
          style={{
            width: 20,
            height: 20,
            tintColor:'#C74F4F'
          }}
        />
  )}}
        component={PromoteOnlineStoreScreenStack}
      />      
    </Drawer.Navigator>
  );
}
