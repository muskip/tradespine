/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import 'react-native-gesture-handler';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import LandingPage from '../screens/LandingPage';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/Login';
import Signup from '../screens/Signup';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const HeaderLeft = props => {
  const toggleDrawer = () => {
    props.navigation.toggleDrawer();
  };

  return (
    <View style={{flexDirection: 'row'}}>
      <TouchableOpacity onPress={() => toggleDrawer()}>
        <Image
          source={require('../assets/images/menu.png')}
          style={{
            width: 20,
            height: 20,
            marginLeft: 10,
            tintColor: '#fff',
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

const HeaderRight = props => {
  return (
    <View style={{flexDirection: 'row', marginEnd: 20}}>
      <TouchableOpacity>
        <Image
          source={require('../assets/images/location.png')}
          style={{
            width: 20,
            height: 20,
            marginEnd: 20,
            tintColor: '#fff',
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          source={require('../assets/images/search.png')}
          style={{
            width: 20,
            height: 20,
            marginEnd: 20,
            tintColor: '#fff',
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
        <Text style={{color: '#fff'}}>LOGIN</Text>
      </TouchableOpacity>
    </View>
  );
};

function firstScreenStack({navigation}) {
  return (
    <Stack.Navigator initialRouteName="LandingPage">
      <Stack.Screen
        name="LandingPage"
        component={LandingPage}
        options={{
          title: 'LOGO', //Set Header Title
          headerLeft: () => <HeaderLeft navigation={navigation} />,
          headerRight: () => <HeaderRight navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#F4717E', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Signup"
        component={Signup}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

export default function SideDrawer() {
  return (
    <Drawer.Navigator
      drawerStyle={{marginTop: 55, paddingHorizontal: 10}}
      drawerContentOptions={{
        activeTintColor: '#C74F4F',
        itemStyle: {marginVertical: 5},
      }}>
      <Drawer.Screen
        name="FirstPage"
        options={{drawerLabel: 'First Page'}}
        component={firstScreenStack}
      />
      <Drawer.Screen
        name="SecondPage"
        options={{drawerLabel: 'Second Page'}}
        component={firstScreenStack}
      />
    </Drawer.Navigator>
  );
}
