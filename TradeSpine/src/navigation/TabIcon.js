/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {Image, StyleSheet} from 'react-native';

export function TabIcon({focused, image}) {
  return (
    <Image
      style={[styles.default_tab_icon, {tintColor: '#FFFF'}]}
      source={image}
    />
  );
}

const styles = StyleSheet.create({
  default_tab_icon: {
    height: 22,
    width: 22,
    alignSelf: 'center',
    marginVertical: 5,
  },
  active_tab_icon: {
    height: 22,
    width: 22,
    alignSelf: 'center',
    marginVertical: 5,
  },
});
