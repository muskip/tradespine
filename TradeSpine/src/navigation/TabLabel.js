/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {View, Text} from 'react-native';

export function TabLabel({focused, label}) {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Text style={{color: '#FFFF', textAlign: 'center', fontSize: 12}}>
        {label}
      </Text>
    </View>
  );
}
