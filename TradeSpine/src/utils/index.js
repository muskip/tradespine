import ImagePicker from 'react-native-image-crop-picker';

export const isTextEmpty = text => {
  return !text.trim();
};

const defaultImagePickerOption = {
  width: 300,
  height: 300,
  cropping: true,
  includeBase64: true,
  compressImageQuality: 1,
  cropperCircleOverlay: true,
};

export const pickImage = async (
  type = 'camera',
  options = defaultImagePickerOption,
) => {
  try {
    return type === 'camera'
      ? ImagePicker.openCamera(options)
      : ImagePicker.openPicker(options);
  } catch (error) {}
};

export function getAdsIndex(index) {
  let adsIndex = 0;
  switch (index) {
    case 2:
      adsIndex = 0;
      break;

    case 5:
      adsIndex = 1;
      break;

    case 8:
      adsIndex = 2;
      break;

    case 11:
      adsIndex = 3;
      break;

    case 14:
      adsIndex = 4;
      break;
    case 17:
      adsIndex = 5;
      break;
    case 20:
      adsIndex = 6;
      break;
    case 23:
      adsIndex = 7;
      break;
    case 26:
      adsIndex = 8;
      break;
    case 29:
      adsIndex = 9;
      break;
    case 32:
      adsIndex = 10;
      break;

    default:
      adsIndex = 0;
  }
  return adsIndex;
}
